import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../Assets/theme/color';
import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';

const {height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
  container: {
    height: Platform.OS === 'ios' ? responsiveHeight(9) : responsiveHeight(7.5),
    position: 'relative',
    backgroundColor: 'white',
  },
  font: {
    fontSize: height / 40,
    display: 'flex',
    fontFamily: 'Poppins-semibold',
    marginTop: Platform.OS === 'ios' ? height / 25 : height / 45,
    alignSelf: 'center',
  },

  viewparent: {
    height: responsiveHeight(152),
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    backgroundColor: '#F1F2F6',
    marginTop: 5,
  },

  bouton: {
    height: responsiveHeight(8),
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
  },
  bouton2: {
    height: height / 10,
    marginTop: height / 54,
  },
});
