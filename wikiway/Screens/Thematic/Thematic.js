import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  Text,
  ScrollView,
} from 'react-native';
import {styles} from './Styles/Thematic';
import TopBar from '../../Components/TopBar';
import BulleThematique from '../../Components/BubbleThematic';
import ContinuerBouton from '../../Components/NextButton';
import {updatethematic, updatecelebrite, getAllLieux, updatedelay} from '../../actions';
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';


const thematic = props => {
  const lieuxe = useSelector(state => state.lieux);
  const dispatch = useDispatch();
  const [thematictableau, setThematictableau] = useState([]);
  const {t} = useTranslation();
  const [Celebritetableau, setCelebritetableau] = useState([]);
  const lenghtTableau = thematictableau.length + Celebritetableau.length;
  //console.log(lenghtTableau)


  const Continuer = () => {
    dispatch(updatethematic(thematictableau));
    dispatch(updatecelebrite(Celebritetableau));
    props.navigation.navigate('NavBar');
  };



  const togglethematic = nom => {
    const thematic = thematictableau.findIndex(element => element == nom);

    if (thematic == -1) {
      if (lenghtTableau == 1 ) {
        return alert('One thematic only');
      }
     
      return setThematictableau([...thematictableau, nom]);
      
    }

    setThematictableau([
      ...thematictableau.slice(0, thematic),
      ...thematictableau.slice(thematic + 1),
    ]);
  };

  const togglecelebrite = nom => {
    const celebrite = Celebritetableau.findIndex(element => element == nom);

    if (celebrite == -1) {
      if (lenghtTableau == 1 ) {
        return alert('One thematic only');
      }
      return setCelebritetableau([...Celebritetableau, nom]);
      
    }

    setCelebritetableau([
      ...Celebritetableau.slice(0, celebrite),
      ...Celebritetableau.slice(celebrite + 1),
    ]);
  };


  // fonctions for setup if the thematic is choose or not  
  const celebritetruefalse = nom => {
    return Celebritetableau.includes(nom);
  };
 
  const thematictruefalse = nom => {
    return thematictableau.includes(nom);
  };

  return (
    <>
      <View style={styles.container}>
        {/* <TopBar titre={"Thématiques"} onPress={() => thematique("all")} value={(généralité == true && geographie == true && urbanisme == true && transport == true && toponymie == true && histoire == true && politique == true && culture == true) ? true : false}/> */}
        <Text style={styles.font}>{t('thematiquePage.topbar.thematique')}</Text>
      </View>
      <ScrollView style={styles.viewparent2}>
        <View style={styles.viewparent}>
          <BulleThematique
            titre={t('thematiquePage.bubbleButton.generalite')}
            image={require('../../Assets/Images/Themes/Ville.png')}
            onPress={() => togglethematic('city')}
            value={thematictruefalse('city')}
          />
          <BulleThematique
            titre={t('thematiquePage.bubbleButton.transport')}
            image={require('../../Assets/Images/Themes/circ2.png')}
            onPress={() => togglethematic('transport_zone')}
            value={thematictruefalse('transport_zone')}
          />
           <BulleThematique
            titre={t('thematiquePage.bubbleButton.urbanisme')}
            image={require('../../Assets/Images/Themes/commerciales.png')}
            onPress={() => togglethematic('commercial_zone_and_industry')}
            value={thematictruefalse('commercial_zone_and_industry')}
          />
          <BulleThematique
            titre={t('thematiquePage.bubbleButton.service')}
            image={require('../../Assets/Images/Themes/livre.png')}
            onPress={() => togglethematic('etudes_sante')}
            value={thematictruefalse('etudes_sante')}
          />
          <BulleThematique
            titre={t('thematiquePage.bubbleButton.culture')}
            image={require('../../Assets/Images/Themes/loisirs.png')}
            onPress={() => togglethematic('Leisure_and_relaxation_areas')}
            value={thematictruefalse('Leisure_and_relaxation_areas')}
          />
          <BulleThematique
            titre={t('thematiquePage.bubbleButton.histoire')}
            image={require('../../Assets/Images/Themes/culture.png')}
            onPress={() => togglethematic('Places_of_culture_and_history')}
            value={thematictruefalse('Places_of_culture_and_history')}
          />
          <BulleThematique
            titre={t('thematiquePage.bubbleButton.politique')}
            image={require('../../Assets/Images/Themes/pouvoir.png')}
            onPress={() => togglethematic('place_of_power')}
            value={thematictruefalse('place_of_power')}
          />
          <BulleThematique
            titre={t('thematiquePage.bubbleButton.events')}
            image={require('../../Assets/Images/Themes/events.png')}
            onPress={() => togglethematic('Lieux_events')}
            value={thematictruefalse('Lieux_events')}
          />
          <BulleThematique
            titre={t('thematiquePage.bubbleButton.geographie')}
            image={require('../../Assets/Images/Themes/foret.png')}
            onPress={() => togglethematic('Natural_spaces')}
            value={thematictruefalse('Natural_spaces')}
          />
          <BulleThematique
            titre={t('thematiquePage.bubbleButton.culte')}
            image={require('../../Assets/Images/Themes/culte.png')}
            onPress={() => togglethematic('lieux_culte')}
            value={thematictruefalse('lieux_culte')}
          />
          <BulleThematique 
            titre={t('thematiquePage.bubbleButton.place')}
            image={require('../../Assets/Images/Themes/places.png')}
            onPress={() => togglethematic('place_and_street')}
            value={thematictruefalse('place_and_street')}
          />
          <BulleThematique 
            titre={t('thematiquePage.bubbleButton.celebrite')}
            image={require('../../Assets/Images/Themes/celebrite.png')}
            onPress={() => togglecelebrite('celebrite')}
            value={celebritetruefalse("celebrite")}
          />
          <BulleThematique
            titre={t('thematiquePage.bubbleButton.eau')}
            image={require('../../Assets/Images/Themes/vague2.png')}
            onPress={() => togglethematic('eau')}
            value={thematictruefalse('eau')}
          />
          <BulleThematique 
            titre={t('thematiquePage.bubbleButton.archeologie')}
            image={require('../../Assets/Images/Themes/grotte.png')}
            onPress={() => togglethematic('archeologie')}
            value={thematictruefalse("archeologie")}
          />
        </View>
      </ScrollView>
      <View style={styles.bouton}>
        <ContinuerBouton
          text={lenghtTableau != 0 ? t('thematiquePage.NextButton.continuer') : t('thematiquePage.NextButton.toutcontinuer')}
          onPress={() => Continuer()}
        />
      </View>
    </>
  );        
};

export default thematic;
