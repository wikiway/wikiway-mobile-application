import SplashScreen from './BottomBarScreen/SplashScreen/SplashScreen';
import Sliders from './BottomBarScreen/Sliders/Sliders';
import Thematic from './Thematic/Thematic';
import Home from './BottomBarScreen/Home/Home';
import Voice from './BottomBarScreen/WikiAudio/WikiAudio';
import Retro from './BottomBarScreen/Retro/Retro';
import Parametres from './BottomBarScreen/Parametres/Parametres';
import detailLieu from './BottomBarScreen/Home/DetailLieu';
import Form from './BottomBarScreen/Home/Form';
import Langue from './BottomBarScreen/Parametres/Langue'
import Voix from './BottomBarScreen/Parametres/Voix'
import Luminosite from './BottomBarScreen/Parametres/Luminosite'
import PolitiqueConfidentialite from './BottomBarScreen/Parametres/PolitiqueConfidentialite'
import ConditionUtilisation from './BottomBarScreen/Parametres/ConditionUtilisation'
import MentionsLegales from './BottomBarScreen/Parametres/MentionsLegales';
import DescThematic from './BottomBarScreen/Parametres/DescThematic';
import WebView1 from './BottomBarScreen/WebView/WebView1';
import Sliders2 from './BottomBarScreen/Parametres/Sliders2';

export {
    SplashScreen,
    Sliders,
    Thematic,
    Home,
    DescThematic,
    Voice,
    Retro,
    Parametres,
    detailLieu,
    Form,
    Langue, 
    Sliders2,
    Voix,
    WebView1,
    Luminosite,
    PolitiqueConfidentialite,
    ConditionUtilisation,
    MentionsLegales,
};