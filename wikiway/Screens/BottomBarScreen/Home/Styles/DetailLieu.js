import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.background,
      },
    
      scroll: {
        marginTop: Platform.OS === 'ios' ? height / 8 : height / 12,
      },
      wikipedialogo:{
        width: responsiveHeight(9),
        height: responsiveHeight(9),
      },
      image: {
        width: responsiveHeight(65),
        height: responsiveHeight(30),
      },
    
      left: {
        width: width / 3,
        height: height / 14,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#CFC2C2',
      },
    
      containerPosition1: {
        width: width,
        height: responsiveHeight(30),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      
      },
    
      son: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: height / 70,
        
      },
    
      textdiv: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 30,
      },
      
      text1: {
        fontFamily:'Poppins-Bold',
        fontSize: responsiveFontSize(2.7),
        textAlign:'justify',
        marginBottom: height / 20,
      },
    
      text2: {
        marginTop: height / 50,
        textAlign: 'justify',
        marginBottom: height / 70,
        fontFamily: Platform.OS === 'ios' ?'Poppins-ExtraLight': 'Poppins-light',
      },
      text3: {
        marginBottom: height / 50,
        textAlign: 'justify',
        fontWeight: 'bold',
        fontSize: responsiveFontSize(2),
        fontFamily:'Poppins-Medium',
      },

      containerLoading: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
      },


      text20: {
        fontWeight: 'bold',
        fontSize: responsiveFontSize(2.2),
        fontFamily:'Poppins-Medium',
        textAlign: 'justify',
       
      },
    
      text21: {
        textAlign: 'justify',
        marginBottom: height / 60,
        fontSize: responsiveFontSize(2),
        fontFamily:Platform.OS === 'ios' ?'Poppins-ExtraLight': 'Poppins-light',
     }
});