import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';
const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.background,
  },

  scroll: {
    marginTop: Platform.OS === 'ios' ? height / 8 : height / 8,
  },

  noresultcontainer: {
    width: width,
    height: height,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  containerPosition2: {
    width: width,
    height: height / 15,
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: height / 45,
    position: 'relative',
  },
  bouton: {
    height: height / 20,
    width: width / 8,
    position: 'absolute',
    right: 10,
    bottom: 7,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bouton5: {
    height: height / 20,
    width: width / 8,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  text2: {
    padding: responsiveHeight(2.5),
    textAlign: 'center',
    fontSize: responsiveFontSize(2.1),
  },
  containercard: {
    marginTop: responsiveHeight(4.5),
    marginBottom: height / 50,
  },
  texteInfos: {
    paddingTop: Platform.OS === 'ios' ? responsiveHeight(3) : 0,
    marginLeft: responsiveWidth(2),
    fontFamily: 'Poppins-Bold',
    color: COLORS.textColor,
    fontSize: responsiveFontSize(1.9),
  },

  card: {
    width: width,
    marginTop: height / 80,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },

  displays: {
    width: responsiveWidth(65),
    display: 'flex',
    padding: 4,
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    bottom: 3,
    // backgroundColor:'blue'
  },

  // distance: {
  //   marginRight: width / 4,
  // },

  left: {
    width: width / 3,
    height: responsiveHeight(14),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: COLORS.blacklight,
  },
  image: {
    width: '100%',
    height: '100%',
  },

  right: {
    backgroundColor: 'white',
    width: responsiveWidth(67),
    height: responsiveHeight(14),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: COLORS.blacklight,
    position: 'relative',

    // position:'relative',
  },

  texteNomLieu: {
    marginTop: responsiveHeight(2.5),
    fontFamily: 'Poppins-Medium',
    color: COLORS.black,
    fontSize: responsiveFontSize(1.8),
    textAlign: 'center',
  },

  paragraph: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: responsiveFontSize(2),
  },

  thematics: {
    fontFamily: 'Poppins-Italic',
    color: COLORS.thematicTextColor,
  },

  distance: {
    fontFamily: 'Poppins-Medium',
    fontSize: responsiveFontSize(1.7),
  },

  boutontop: {
    backgroundColor: 'orange',
    width: width,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  containerLoading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },

  centre: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: height / 9,
    flex: 1,
  },

  textdiv: {
    width: width,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },

  greytext: {
    textAlign: 'center',
  },

  loadIcon: {
    width: width,
  },

  cit: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginRight: 18,
  },

  containerend: {
    height: responsiveHeight(4),
    marginTop: responsiveHeight(1),
    marginBottom: responsiveHeight(3),
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },

  arrow: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  arrowRight: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: responsiveHeight(3),
    width: responsiveHeight(8),
  },

  arrowLeft: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: responsiveHeight(3),
    width: responsiveHeight(8),
  },
});
