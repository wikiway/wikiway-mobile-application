import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  globalview: {
    marginTop: Platform.OS === 'ios' ? height / 8 : height / 15,
    padding: width / 23,
  },
  text: {
    marginTop: responsiveHeight(5),
    fontSize: height / 55,
  },
  text1: {
    fontSize: height / 65,
    marginTop: height / 35,
    marginBottom: height / 35,
  },
  marginput: {
    marginBottom: height / 35,
  },
  container2: {
    flex: 1,
  },
  contentcolor2: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  modals2: {
    backgroundColor: COLORS.white,
    flex: 1,
    display: 'flex',
    paddingRight: responsiveHeight(4),
    paddingLeft: responsiveHeight(4),

  },
  transpa: {
    height: responsiveHeight(29.5),
    width: width,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  divdeco: {
    display: 'flex',
    height: responsiveHeight(15),
    paddingBottom: responsiveHeight(2)
    },
  textdeco: {
    paddingTop: responsiveHeight(2),
    fontSize: responsiveHeight(3),
    textAlign: "center",
    fontFamily:'Poppins-Bold'
  },
  row1: {
    flexDirection:'row',
    justifyContent:'space-between',
  },
  logo: {
    height: height / 15,
    width: width,
    marginBottom: height / 35,
  },
  close: {
    display: 'flex',
  },

  center: {
    flexDirection:'row',
    justifyContent: "space-between",
    marginTop: responsiveHeight(2),
    paddingRight: responsiveHeight(0)
  },

  itemStyle:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-between',
    marginTop: responsiveHeight(4),
    
  },
  centerText:{
    alignSelf:'center'
  },
  textModalitem:{
    fontSize: responsiveHeight(2.7),
    fontFamily:'Poppins-Medium'
    

  },
});