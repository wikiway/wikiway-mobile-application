import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: COLORS.background,
       
      },
      scroll: {
        marginTop: Platform.OS === 'ios' ? height / 8 : height / 12,
        marginBottom: height / 50,
      },
      input: {
        // padding: 10,
        width: width / 1.4,
        // textAlign: 'center',
        backgroundColor: COLORS.background,
        
      },
      inputW: {
        // padding: 10,
        width: width/1.4,
        backgroundColor: COLORS.background,
        
      },
      balisetext:{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        
      },

      text: {
        fontSize:20,
      },

      div: {
        marginBottom: height / 30,
        marginTop: height/30,
      },

      div2: {
        display: 'flex',
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: height / 70,
        marginTop: height/50,
        marginLeft: width/30,
        marginRight: width/30
      },
      Mailinput: {
          width: width,
          display: 'flex',
          flexDirection:'row',
          alignItems: 'center',
          justifyContent: 'center',
      },
      wikipediainput: {
        height: height / 10,
        width: width / 1.2,
        backgroundColor: COLORS.white,
        borderWidth: width/ 160,
        borderRadius: 50,
        display: 'flex',
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
      },
      bouton2: {
        alignItems: 'center',
        marginRight: width / 18,
        
      },
    
      Objetinput:{
        marginTop: height/25,
        width: width,
        display: 'flex',
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
        
      },
      descriptioninput:{
         width: width / 1.2,
         backgroundColor: COLORS.white,
         textAlignVertical:'top',
         paddingBottom:0,
        //  height:height/3.6,
        //  padding: 20,
       
    },
    button3:{
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      bottom: 0,
    },

  
 
});