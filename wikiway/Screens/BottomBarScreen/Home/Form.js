import React, {useState, useCallback, useEffect} from 'react';
import axios from 'axios';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
  Linking
} from 'react-native';
import * as Components from '../../../Components/index';
import EmailIcon from '../../../Assets/Images/Form/mail.svg';
import ObjetIcon from '../../../Assets/Images/Form/globe.svg';
import {TextInput} from 'react-native-paper';
import {styles} from './Styles/Form';
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {lieuxConstants} from '../../../actions/constants';
import {articleid, getLieuxbyid} from '../../../actions';
import detailLieu, {gettestarticle} from './DetailLieu';
import WikipediawIcon from '../../../Assets/Images/Form/wikipediaw.svg';

const {width, height} = Dimensions.get('screen');

date = new Date();
annee = date.getFullYear();
mois = date.getMonth() + 1;
jour = date.getDate();
heure = date.getHours();
minute = date.getMinutes();

const date_heure1 = jour + '/' + mois + '/' + annee + ' à ' + heure + ' h: ' + minute;
const date_heure = date_heure1.toString();
//const url1 = lieuxe.title; import { useDispatch, useSelector } from 'react-redux';

const Form = props => {
  const lieuxe = useSelector(state => state.lieux);
  const {t} = useTranslation();
  const [isLoading, setIsLoading] = useState(false);
  const [title, setTitle] = useState('');
  const [message, setMessage] = useState('');
  const [description, setDescription] = React.useState('');
  const [aff, setAff] = useState(false);
  const [aff2, setAff2] = useState(false); //En cas d'échec
  const [ActiveDescription, setActiveComment] = useState(false);
  const dispatch = useDispatch();

  const url = lieuxe.title
  
  const validate = () => {
      if (title.length > 2) {
        if (message.length > 5) {
          const response = axios
              .post('https://api.wikiway.guide/errors', {
                title,
                message,
                url,
                date_heure,
              })
              .then(function (response) {
                // handle success
                console.log(JSON.stringify(response.data));
              })
              .catch(function (error) {
                console.log(JSON.stringify(response.data));
                // handle error
                alert(error.message);
              });
          setAff(true);
          //faire la requête ici
          setTimeout(() => {
            setAff(false);
            props.navigation.navigate('detailLieu');
          }, 3000);
        } else {
          alert('Le champs Objet doit contenir au moins 3 characteres');
        }
      } else {
        alert('Le champs Description doit contenir au moins 5 characteres');
      }
  };
  activeComment = () => {
    setActiveComment(true);
  };

  const onchangetexttitle = title => {
    setTitle(title);
  };
  const onchangemessage = message => {
    setMessage(message);
  };
  useEffect(() => {
    console.log('titre passé', lieuxe.title);
    dispatch(getLieuxbyid(lieuxe.title, lieuxe.thematic));
  }, []);

  return (
    <View style={styles.container}>
      <Components.TopBar
        titre="Form"
        // onPress={() => props.navigation.navigate('detailLieu')}
        onPress={() => props.navigation.goBack()}
      />

      <ScrollView style={styles.scroll}>
        <View style={styles.div}>
          <View style={styles.Mailinput}>
            <TouchableOpacity style={styles.bouton2} onPress={props.onPress1}>
              <EmailIcon width={width / 9} height={height / 18} />
            </TouchableOpacity>

            <TextInput
              label={t('object')}
              value={title}
              onChangeText={onchangetexttitle}
              style={styles.input}
              theme={{
                colors: {
                  primary: '#45C6BE',
                  borderWidth: '1',
                  text: 'black',
                  placeholder: '#AFAFAF',
                  borderBottomColor: 'black',
                },
              }}
            />
          </View>
          <View style={styles.Objetinput}>
            <View style={styles.Objetinput}>
              <TouchableOpacity onPress={() => activeComment()}>
                <TextInput
                  multiline={true}
                  value={message}
                  mode="outlined"
                  label={t('message')}
                  onChangeText={onchangemessage}
                  style={styles.descriptioninput}
                  height={height / 3.6}
                  theme={{
                    colors: {
                      primary: '#45C6BE',
                      borderWidth: '0',
                      text: 'black',
                      placeholder: '#AFAFAF',
                    },
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={styles.button3}>
        <Components.Button
          type="return"
          text={t('FormPage.retour')}
          onPress={() => props.navigation.navigate('detailLieu')}
        />
        {console.log('description', description)}
        <Components.Button
          type="check"
          value={title.length >= 1 && message.length >= 1 ? true : false}
          text={t('FormPage.envoyer')}
          onPress={validate}
        />
      </View>
      <View>
        <Components.ValidationFormulaire
          value={aff}
          text={t('FormPage.envoi')}
        />
      </View>
      <View>
        <Components.RefusFormulaire
          value={aff2}
          text="L'envoie de votre message a échoué"
        />
      </View>
    </View>
  );
};

export default Form;
