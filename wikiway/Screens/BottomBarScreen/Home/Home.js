import React, {useState, useEffect, useRef} from 'react';
import {useIsFocused, useFocusEffect} from '@react-navigation/native';
import ReactPaginate from 'react-paginate';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  Alert,
  Linking,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Components from '../../../Components/index';
import {Constants} from 'react-native-unimodules';
import * as Location from 'expo-location';
import MapIcon from '../../../Assets/Images/CurrentPosition/map.svg';
import NotImageIcon from '../../../Assets/Images/Card/notImage.svg';
import {styles} from './Styles/Home';
import {useDispatch, useSelector} from 'react-redux';
import {getAllLieux, updatetitle, updatelonglat, articleid, pagecounter} from '../../../actions';
import {useTranslation} from 'react-i18next';
import * as IntentLauncher from 'expo-intent-launcher';
import LoadIcon from '../../../Assets/Images/CurrentPosition/load.svg';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';

const {width, height} = Dimensions.get('screen');

const Home = props => {
  const isFocused = useIsFocused();
  const navigation = useNavigation();
  const {t} = useTranslation();
  const lieuxe = useSelector(state => state.lieux);
  const [pageCount, setPageCount] = useState(lieuxe.page);
  const [location, setLocation] = useState(null);
  const [Adress, setAdress] = useState(t('HomePage.attente'));
  const [statut, setstatut] = useState(null);
  const [isLoading, setisLoading] = useState(lieuxe.loading);
  const ref = useRef(null);
  const dispatch = useDispatch();

  checklocation = async () => {
    try {
      let {status} = await Location.requestForegroundPermissionsAsync(); //Cette fonction est appele si seulement la geolocalisation est activé
      // let ndcheck = await Location.enableNetworkProviderAsync()
      // console.log(ndcheck)

      if (status == 'granted') {
        setstatut(true);

        // let location = await Location.getCurrentPositionAsync({
        //   account: Location.Accuracy.BestForNavigation,
        // });

        await Location.watchPositionAsync(
          {accuracy: Location.Accuracy.High},
          position => {
            //console.log('positions', position.coords);
            const latitude = position.coords.latitude;
            const longitude = position.coords.longitude;
            dispatch(updatelonglat(longitude, latitude));
            console.log(latitude,longitude)
            reverse(latitude,longitude)
          },
        );

        //setLocation(location);
        // console.log(location)
        //  console.log('localisation',location)
      } else if (status !== 'granted') {
        setstatut(false);

        Alert.alert(
          'WikiWay est plus efficace lorsque le service de localisation est activé',
          'En activant le service de localisation, vous obtiendrez des informations plus précises et performantes',
          [
            {
              text: 'Activer dans les paramètres',
              onPress: () => goToSettings(),
              // style: 'cancel',
            },
            {
              text: 'Maintenir le service de localisation désactivé',
              onPress: () => props.navigation.navigate('NavBar'),
            },
          ],
        );
      }
    } catch (e) {
      setstatut(false);
    }
  };
  
  useEffect(() => {
    reverse = async (latitude, longitude) => {
      let response = await Location.reverseGeocodeAsync({
        latitude,
        longitude,
      });
      for (let item of response) {
        let address = ` ${item.city},${item.postalCode}`;
        // let address = `${item.name}, ${item.street}, ${item.postalCode}, ${item.city};`;

        setAdress(address);
      }
    };

    checklocation();
    //setInterval(getAllLieuthe, 10000)
  }, []);

  const arrowleft = () => {
    setPageCount(pageCount => pageCount - 20)
    let page = pageCount - 20
    dispatch(pagecounter(page)) //page
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        lieuxe.language,
        lieuxe.celebrit,
      ),
    );
  };

  const arrowright = () => {
    setPageCount(pageCount => pageCount + 20)
    let page = pageCount + 20
    dispatch(pagecounter(page)) //page
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        lieuxe.language,
        lieuxe.celebrit,
      ),
    );
  };

  dis = () => {
    setPageCount(pageCount => pageCount = 0)
    let page = 0
    dispatch(pagecounter(page)) // tester qu'avec le setpagecount voir si on peut reduire 
    console.log("fdazgfg" + lieuxe.language)
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        lieuxe.language,
        lieuxe.celebrit,
      ),

    )

  }, [lieuxe.latitude , lieuxe.longitude];

  useEffect(() => {
    
    if (location === null){
      console.log('toto\n\n' + lieuxe.language)
      if (lieuxe.longitude == null || lieuxe.latitude == null) return;
      
      dis()
      setLocation(1);

    }
  },[lieuxe.latitude, lieuxe.longitude]);

  /*useEffect(() => {
    if (isFocused){
      dis()
    }
  },[isFocused]);*/
  //const getAllLieuthe = dispatch(getAllLieux(lieuxe.radius,lieuxe.thematic,lieuxe.longitude,lieuxe.latitude,lieuxe.language))

  goToSettings = () => {
    if (Platform.OS == 'ios') {
      // Linking for iOS
      Linking.openURL('app-settings:');
      // Linking.openURL('App-Prefs:root=privacy');
    } else {
      // IntentLauncher for Android
      IntentLauncher.startActivityAsync(
        IntentLauncher.ACTION_LOCATION_SOURCE_SETTINGS,
      );
    }
  };

  const detailLieu = (value, value1) => {
    dispatch(updatetitle(value, value1));
    dispatch(articleid(value))
    props.navigation.navigate('detailLieu');
  };

  GoToThematic = () => {
    navigation.navigate('Thematic');
    setLocation(null);
  };

  const Capitalize = str => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };


  if (lieuxe.loading == true) {
    return (
      <View style={styles.containerLoading}>
        <Components.TopBar titre="Accueil2" />
        <ActivityIndicator animating={true} size="large" color='#45C6BE'  />
      </View>
    );
  } else {
    return (
      <View style={styles.container}>
        <Components.TopBar titre="Accueil"
        onPress2 ={() => props.navigation.navigate('Thematic')} />

        {lieuxe.error != null ? (
          <View style={styles.centre}>
            <View style={styles.textdiv}>
              <Text style={styles.greytext}>
                {t("HomePage.reseau")}
              </Text>
              {/* <Text style={styles.greytext}>{lieuxe.error}</Text> */}
            </View>
          </View>
        ) : lieuxe.lieux.length > 0 ? (
          <ScrollView style={styles.scroll}>
            {statut ? null : (
              <TouchableOpacity
                onPress={() => goToSettings()}
                style={styles.boutontop}>
                <Text>{t("HomePage.click")}</Text>
              </TouchableOpacity>
            )}
              <Text style={styles.texteInfos}>
                {t('HomePage.positionactuelle')}
              </Text>
              <View style={styles.containerPosition2}>
                <>
                  <View style={styles.containerinterne1}>
                    {/* <Text> activé </Text> */}
                    <Text style={styles.paragraph}>{Adress}</Text>
                  </View>
                </>
              </View>
            <View style={styles.containercard}>
              <View style={styles.cit}>
              <Text style={styles.texteInfos}>
                {t('HomePage.info')} {lieuxe.radius} Km
              </Text>
              <TouchableOpacity onPress={()=> dis()}>
              <LoadIcon
                width={responsiveHeight(4.2)}
                height={responsiveHeight(4.5)}
              />
              </TouchableOpacity>
              </View>
              {lieuxe.lieux.map((value, index) => (
                <TouchableOpacity
                  style={styles.card}
                  onPress={() =>
                    detailLieu(value.article, value._links.image.self)
                  }
                  key={index}>
                  <View style={styles.left}>
                    {/* {value._links.image.self != null ? <Image style={styles.image}  source={{uri: value._links.image.self }} />  : <Image source={require('../../../Assets/Images/Card/notImage.png')} style={styles.image}/>} */}
                    {/* {isLoading ? <ActivityIndicator size="large" /> : value._links.image.self != null ? <Image style={styles.image}  source={{uri: value._links.image.self }} />  :  <NotImageIcon style={styles.iconmap} width={width /2}/> } */}
                    {value._links.image.self != null ? (
                      <Image
                        style={styles.image}
                        source={{uri: value._links.image.self}}
                      />
                    ) : (
                      <NotImageIcon style={styles.iconmap} width={width / 2} />
                    )}
                  </View>
                  <View style={styles.right}>
                    <Text style={styles.texteNomLieu}>
                      {Capitalize(value.title)}
                    </Text>

                    <View style={styles.displays}>
                      <Text style={styles.distance}>
                        {value.distance * 1000 < 1000 ? (
                          <Text>{value.distance * 1000} m</Text>
                        ) : (
                          <Text>{value.distance} km</Text>
                        )}
                      </Text>
                      <Text style={styles.thematics}>
                      </Text> 
                    </View>
                  </View>
                </TouchableOpacity>
                
              ))}
            </View>

            <View style={styles.containerend}>
              {lieuxe.page == 0 ? <></> : (<TouchableOpacity style={styles.arrow}  onPress={() => arrowleft()}>
              <Image style={styles.arrowRight} source={require("../../../Assets/Images/Card/Flecheleft.png")} />
              </TouchableOpacity>)}

              <TouchableOpacity style={styles.arrow} onPress={() => arrowright()}>
              <Image style={styles.arrowRight} source={require("../../../Assets/Images/Card/Fleche.png")} />
              </TouchableOpacity>
              
              </View>
          </ScrollView> /* thematiques */
        ) : (
          <>
          {lieuxe.page == 0 ?(
            
                <View style={styles.centre}>
                  <View style={styles.textdiv}>
                    <Text style={styles.greytext}>
                      {t("HomePage.information")}
                    </Text>
                  </View>
                </View>
            
            ):(
              <View style={styles.noresultcontainer}>
                <Text style= {styles.text2}>{t('HomePage.back')}</Text>
                <TouchableOpacity style={styles.arrow}  onPress={() => arrowleft()}>
                <Image style={styles.arrowRight} source={require("../../../Assets/Images/Card/Flecheleft.png")} />
                </TouchableOpacity>
                <Text style= {styles.text2}>
                {t('HomePage.reload')}
                </Text>
                <TouchableOpacity onPress={()=> dis()}>
                  <LoadIcon
                    width={responsiveHeight(5) }
                    height={responsiveHeight(5)}
                  />
                </TouchableOpacity>
              </View>
            )
          }
        </>
      )}

      {/* {lieuxe.error != null ? <Text>Une erreur de serveur ou de réseau est survenue</Text> : null} */}
    </View>
    );
  }
};

export default Home;
