import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

const {width, height} = Dimensions.get('screen');

import * as Components from '../../../Components/index'
import CloseIcon from '../../../Assets/Images/Modal/Close.svg';
import {RadioButton} from 'react-native-paper';
import {styles} from './Styles/RayonModal';
import {useTranslation} from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { getAllLieux, pagecounter } from '../../../actions';
import { updateradius } from '../../../actions';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';

const RayonModal = props => {
  const {t} = useTranslation();
  const [checked, setChecked] = React.useState('first');
  const [valeur, setValeur] = useState(0);
  const lieuxe = useSelector(state => state.lieux);
  const [valeurreq, setValeurreq] = useState(lieuxe.radius);
  const ExitModal = () => {
    setValeur(valeur + 1);
  };
  const dispatch = useDispatch();

  mod = () => {
    let page = 0
    dispatch(pagecounter(page))
    dispatch(updateradius(valeurreq))
    dispatch(getAllLieux(valeurreq,lieuxe.thematic, page, lieuxe.longitude,lieuxe.latitude, lieuxe.language,lieuxe.celebrit))
    ExitModal()
  }
  return (
    <View>
      <View style={styles.container}>
        <Modal transparent={true} visible={props.value > valeur ? true : false}>
          <TouchableOpacity
            style={styles.transpa}
            onPress={() => ExitModal()}></TouchableOpacity>
          <View style={styles.contentcolor2}>
            <View style={styles.modals2}>
              <View style={styles.divdeco}>
                <View style={styles.center}>
                  <Text style={styles.textdeco}>
                    {t('RayonModal.choixrayon')}
                  </Text>
                  <TouchableOpacity
                    onPress={() => ExitModal()}>
                    <CloseIcon width={responsiveWidth(10)} height={responsiveHeight(8)} />
                  </TouchableOpacity>
                </View>

                <View style={styles.itemStyle}>
                  <View style={styles.centerText}>
                    <Text style={styles.textModalitem}>1Km</Text>
                  </View>

                  <View
                    style={{
                      backgroundColor:
                      valeurreq  == 1 ? '#45C6BE' : '#C4C4C4',
                      borderRadius: 20,
                    }}>
                    <RadioButton
                      color={valeurreq == 1 ? '#FFFFFF' : null}
                      value={30}
                      status={valeurreq == 1 ? 'checked' : 'unchecked'}
                      onPress={() => setValeurreq(1)}
                    />
                  </View>
                </View>
                <View style={styles.itemStyle}>
                  <View style={styles.centerText}>
                    <Text style={styles.textModalitem}>5Km</Text>
                  </View>

                  <View
                    style={{
                      backgroundColor:
                      valeurreq  == 5 ? '#45C6BE' : '#C4C4C4',
                      borderRadius: 20,
                    }}>
                    <RadioButton
                      color={valeurreq == 5 ? '#FFFFFF' : null}
                      value={30}
                      status={valeurreq == 5 ? 'checked' : 'unchecked'}
                      onPress={() => setValeurreq(5)}
                    />
                  </View>
                </View>

                <View style={styles.itemStyle}>
                  <View style={styles.centerText}>
                    <Text style={styles.textModalitem}>10Km</Text>
                  </View>
                  <View
                    style={{
                      backgroundColor:
                      valeurreq  == 10 ? '#45C6BE' : '#C4C4C4',
                      borderRadius: 20,
                    }}>
                    <RadioButton
                      value= {1}
                      color={valeurreq  == 10 ? '#FFFFFF' : null}
                      status={valeurreq  == 10 ? 'checked' : 'unchecked'}
                      onPress={() => setValeurreq(10)}
                    />
                  </View>
                </View>
                <View style={styles.itemStyle}>
                  <View style={styles.centerText}>
                    <Text style={styles.textModalitem}>30Km</Text>
                  </View>

                  <View
                    style={{
                      backgroundColor:
                      valeurreq  == 30 ? '#45C6BE' : '#C4C4C4',
                      borderRadius: 20,
                    }}>
                    <RadioButton
                      color={valeurreq == 30 ? '#FFFFFF' : null}
                      value={30}
                      status={valeurreq == 30 ? 'checked' : 'unchecked'}
                      onPress={() => setValeurreq(30)}
                    />
                  </View>
                </View>
                <View style={styles.itemStyle}>
                  <View style={styles.centerText}>
                    <Text style={styles.textModalitem}>50Km</Text>
                  </View>

                  <View
                    style={{
                      backgroundColor:
                      valeurreq  == 50 ? '#45C6BE' : '#C4C4C4',
                      borderRadius: 20,
                    }}>
                    <RadioButton
                      color={valeurreq == 50 ? '#FFFFFF' : null}
                      value={30}
                      status={valeurreq == 50 ? 'checked' : 'unchecked'}
                      onPress={() => setValeurreq(50)}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
          <Components.NextButton
            text={t('RayonModal.confirmebutton')}
            onPress={() => mod()}
          />
        </Modal>
      </View>
    </View>
  );
};

export default RayonModal;
