import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
  Share,
  ActivityIndicator,
} from 'react-native';
import * as Components from '../../../Components/index';
import WikipediaIcon from '../../../Assets/Images/Card/wikipediaw.svg';
import { useNavigation } from '@react-navigation/native';
import {styles} from './Styles/DetailLieu';
import NotImageGrandeeIcon from '../../../Assets/Images/Card/notImageGrandee.svg';
import {useDispatch, useSelector} from 'react-redux';
import {getLieuxbyid, articleid} from '../../../actions';
import {useTranslation} from 'react-i18next';

const {width, height} = Dimensions.get('screen');

const detailLieu = props => {
  const navigation = useNavigation();
  const {t} = useTranslation();
  const lieuxe = useSelector(state => state.lieux);
  const dispatch = useDispatch();
  const lieuxarticle = lieuxe.title

  const linkEdit = () =>{
    const lieuxet = lieuxe.title + "?veaction=edit"
    dispatch(articleid(lieuxet))
    navigation.navigate("WebView1")
  }
  
  const link = () =>{
    const lieuxet = lieuxe.title
    dispatch(articleid(lieuxet))
    navigation.navigate("WebView1")
  }

  useEffect(() => {
    dispatch(getLieuxbyid(lieuxe.title, lieuxe.thematic));
  }, []);

  const onShare = async () => {
    try {
      const result = await Share.share({
        message:lieuxarticle,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  if (lieuxe.loading == true) {
    return (
      <View style={styles.containerLoading}>
        <ActivityIndicator size="large" />
      </View>
    );
  } else {
    return (
      <View style={styles.container}>
        <Components.TopBar
          titre="detailLieu"
          onPress={() => props.navigation.navigate('NavBar')}
          onPress0={() => linkEdit()}
          onPress1={() => props.navigation.navigate('Form')}
          onPress2={() => onShare()}
        />
        {lieuxe.lieuxdetail.length > 0 ? (
          <ScrollView style={styles.scroll} scrollIndicatorInsets={{right: 1}}>
            {lieuxe.lieuxdetail.map((value, index) => (
              <View key={index}>
                <View style={styles.containerPosition1}>
                  {lieuxe.image != null ? (
                    <Image source={{uri: lieuxe.image}} style={styles.image} />
                  ) : (
                    <NotImageGrandeeIcon style={styles.image} />
                  )}
                </View>
                <View style={styles.son}>
                  <TouchableOpacity style={styles.bouton3} onPress={() => link()}>
                    <Image style={styles.wikipedialogo} source={require('../../../Assets/Images/Card/600pxw.png')}></Image>
                  </TouchableOpacity>
                  <View style={styles.textdiv}>
                    <Text style={styles.text1}>{value.title}</Text>
                    <Text style={styles.text3}>Introduction </Text>
                        {value.introduction.map((value, index) => (
                        <View key={index}>
                        <Text style={styles.text21}>
                          {value.text0 != null ? value.text0 : (t('Detail_lieu.text0'))}
                        </Text>
                      </View>)
                      )} 

                    <View
                      style={{
                        width: width / 2,
                        marginBottom: 20,
                        borderBottomWidth: 2,
                        borderColor: '#C3B9B9',
                      }}></View>

                    {value.all.map((value, index) => (
                      <View key={index}>
                        <Text style={styles.text20}>
                          {value.title1 != null ? value.title1 : null}
                        </Text>
                        <Text style={styles.text21}>
                          {value.text1 != null ? value.text1 : null}
                        </Text>
                      </View>
                    ))}
                  </View>
                </View>
              </View>
              
            ))}
          </ScrollView>
        ) : (
          <View style={styles.centre}>
            <View style={styles.textdiv}>
              <Text style={styles.greytext}>
                (t('Detail_lieu.information'))
              </Text>
            </View>
          </View>
        )}
      </View>
    );
  }
};
export default detailLieu;
