import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
   
    container: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F1F2F6',
        position: 'relative',
      },
      scroll: {
        marginTop: Platform.OS === 'ios' ? height / 8 : height / 12,
        width: width
      },
      input: {
        width: width / 1.2,
        // shadowColor:'blue',
        // elevation:0
      },
    
      containerPosition1: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: height / 15,
      },
    
      listContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: height / 50,
        
      },
    
      card: {
        width: width / 1.2,
        height: height / 4,
        marginTop: height / 40,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 1,
      },
    
      left: {
        width: width / 1.2,
        height: height / 5.5,
        // borderTopWidth: 1,
        // borderBottomWidth: 1,
        // borderColor: 'blue',
      },
    
      image: {
        flex: 1,
        width: '100%',
        height: '100%',
        // borderRadius:10,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
      },
      right: {
        width: width / 1.2,
        height: height / 14,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
      },
      texteNomLieu: {
        color: '#45C6BE',
        fontFamily:'Poppins-Bold',
        marginLeft: width / 25,
      },
      gridContainer: {
        width: width,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginBottom: height / 50,
        // backgroundColor:'red',
        flexWrap: 'wrap',
      },
    
      card1: {
        width: width / 2.3,
        height: height / 5.5,
        marginTop: height / 40,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 15,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 1,
      },
      top1: {
        width: width / 2.3,
        height: height / 8,
        // borderTopWidth: 1,
        // borderBottomWidth: 1,
        // borderColor: 'blue',
      },
      image1: {
        flex: 1,
        width: '100%',
        height: '100%',
        // borderRadius:10,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
      },
      bottom1: {
        width: width / 2.3,
        height: height / 17,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
      },
      texteNomLieu2: {
        color: '#45C6BE',
        fontFamily:'Poppins-Bold',
        fontSize: 11,
      },

});