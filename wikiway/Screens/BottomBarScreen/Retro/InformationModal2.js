import React, {useState} from 'react';
import {
  View,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  Modal,
  StyleSheet,
  Text,
  Image,
} from 'react-native';

const {width, height} = Dimensions.get('screen');

import * as Components from '../../../Components/index'
import CloseIcon from '../../../Assets/Images/Modal/Close.svg';
import {styles} from './Styles/InformationModal2';
import {RadioButton} from 'react-native-paper';
import RetroLogo from '../../../Assets/Images/TopBar/retroLogos.svg';
import {useTranslation} from 'react-i18next'

const InformationModal2 = props => {
  const {t} = useTranslation();
  const [checked, setChecked] = React.useState('first');

  const [valeur, setValeur] = useState(0);
  const ExitModal = () => {
    setValeur(valeur + 1);
  };

  return (
    <View>
      <View style={styles.container}>
        <Modal transparent={true} visible={props.value > valeur ? true : false}>
          <TouchableOpacity
            style={styles.transpa}
            onPress={() => ExitModal()}></TouchableOpacity>
          <View style={styles.contentcolor2}>
            <View style={styles.modals2}>
              <View style={styles.divdeco}>
                <TouchableOpacity
                  style={styles.close}
                  onPress={() => ExitModal()}>
                  <CloseIcon width={width / 13} height={height / 23} />
                </TouchableOpacity>
                <View style={styles.center}>
                  {/* <View style={styles.logo}>
                    <RetroLogo width={width / 2.5} height={height / 2.5} />
                   </View> */}
                  <Image
                    source={require('../../../Assets/Images/Modal/logo_2.png')}
                    style={styles.image}
                  />
                  <Text style={styles.textdeco}>
                  {t('RetroPageModal.text')}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <Components.NextButton text={t('RetroPageModal.confirmebutton')} onPress={() => ExitModal()} />
          <TouchableOpacity
            style={styles.transpa}
            onPress={() => ExitModal()}></TouchableOpacity>
        </Modal>
      </View>
    </View>
  );
};

export default InformationModal2;
