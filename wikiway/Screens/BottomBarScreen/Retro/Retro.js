import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import TopBar from '../../../Components/TopBar';
import * as Components from '../../../Components/index'
import {Searchbar, shadow} from 'react-native-paper';
import BuildingIcon from '../../../Assets/Images/Retro/Frame.svg';
import {useTranslation} from 'react-i18next'
import {styles} from './Styles/Retro';

const {width, height} = Dimensions.get('screen');

const Retro = props => {
  const {t} = useTranslation();
  const [list, setList] = useState(true);
  const [grid, setGrid] = useState(false);
  const [searchQuery, setSearchQuery] = React.useState('');
  const onChangeSearch = query => setSearchQuery(query);

  const gridFunction = () => {
    setList(false);
    setGrid(true);
  };

  const listFunction = () => {
    setList(true);
    setGrid(false);
  };

  const renderList = () => {
    return (
      <View style={styles.listContainer}>
        <TouchableOpacity style={styles.card} onPress={() => detailLieu()}>
          <View style={styles.left}>
            <Image
              source={require('../../../Assets/Images/Retro/Elysees.png')}
              style={styles.image}
            />
          </View>
          <View style={styles.right}>
            <BuildingIcon width={width / 12} height={height / 16} />
            <Text style={styles.texteNomLieu}>Avenue des champs Elysées</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card} onPress={() => detailLieu()}>
          <View style={styles.left}>
            <Image
              source={require('../../../Assets/Images/Retro/Elysees.png')}
              style={styles.image}
            />
          </View>
          <View style={styles.right}>
            <BuildingIcon width={width / 12} height={height / 16} />
            <Text style={styles.texteNomLieu}>Avenue des champs Elysées</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card} onPress={() => detailLieu()}>
          <View style={styles.left}>
            <Image
              source={require('../../../Assets/Images/Retro/Elysees.png')}
              style={styles.image}
            />
          </View>
          <View style={styles.right}>
            <BuildingIcon width={width / 12} height={height / 16} />
            <Text style={styles.texteNomLieu}>Avenue des champs Elysées</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const renderGrid = () => {
    return (
      <View style={styles.gridContainer}>
        <TouchableOpacity style={styles.card1} onPress={() => detailLieu()}>
          <View style={styles.top1}>
            <Image
              source={require('../../../Assets/Images/Retro/Elysees.png')}
              style={styles.image}
            />
          </View>
          <View style={styles.bottom1}>
            <Text style={styles.texteNomLieu2}>Avenue des champs Elysées</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card1} onPress={() => detailLieu()}>
          <View style={styles.top1}>
            <Image
              source={require('../../../Assets/Images/Retro/Elysees.png')}
              style={styles.image}
            />
          </View>
          <View style={styles.bottom1}>
            <Text style={styles.texteNomLieu2}>Avenue des champs Elysées</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card1} onPress={() => detailLieu()}>
          <View style={styles.top1}>
            <Image
              source={require('../../../Assets/Images/Retro/Elysees.png')}
              style={styles.image}
            />
          </View>
          <View style={styles.bottom1}>
            <Text style={styles.texteNomLieu2}>Avenue des champs Elysées</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card1} onPress={() => detailLieu()}>
          <View style={styles.top1}>
            <Image
              source={require('../../../Assets/Images/Retro/Elysees.png')}
              style={styles.image}
            />
          </View>
          <View style={styles.bottom1}>
            <Text style={styles.texteNomLieu2}>Avenue des champs Elysées</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Components.TopBar titre="retro" />
      <ScrollView style={styles.scroll}>
        <View style={styles.containerPosition1}>
          <Searchbar
            placeholder={t('RetroPage.searchbar')}
            onChangeText={onChangeSearch}
            value={searchQuery}
            iconColor="#DAD9D9"
            style={styles.input}
            theme={{
              colors: {placeholder: '#DAD9D9', text: 'black', primary: 'black'},
            }}
          />
          <Components.HistoryBar
            text={t('RetroPage.historique')}
            onPress={() => listFunction()}
            onPress2={() => gridFunction()}
            value={list}
            value2={grid}
          />
        </View>
          {list ? renderList() : renderGrid()}
      </ScrollView>
    </View>
  );
};

export default Retro;
