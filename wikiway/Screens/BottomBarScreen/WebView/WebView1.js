import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Modal,
  Dimensions,
  StyleSheet,
} from 'react-native';
import { WebView } from 'react-native-webview';
import { styles } from './Styles/WebView1';
import * as Components from '../../../Components/index';
import {useDispatch, useSelector} from 'react-redux';


const WebView1 = props => {
  
  const lieuxe = useSelector(state => state.lieux);

  const link = lieuxe.article

  return (
  <>
    <View style={styles.container2}>
      <Components.TopBar
      style={styles.container2}
      titre="WebView"
      onPress={() => props.navigation.goBack()}
    />
    </View>
    <WebView style={styles.container} source={{uri: link}} 
    startInLoadingState />
  </>
  )
};

export default WebView1;