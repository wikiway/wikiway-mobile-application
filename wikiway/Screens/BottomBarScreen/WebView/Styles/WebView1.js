import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
    container:{
        width: width / 1,
        height:height / 5,
    },
    container2:{
        width: width / 3,
        height:Platform.OS === 'ios' ? height / 9 : responsiveHeight(9),
    }
});
