import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import * as Components from '../../../Components/index'
import {RadioButton} from 'react-native-paper';
import {useTranslation} from 'react-i18next';
import {styles} from './Styles/Luminosite';

const {width, height} = Dimensions.get('screen');

const Luminosite = props => {
  const {t} = useTranslation();
  const [checked, setChecked] = React.useState('first');

  return (
    <View style={styles.container}>
      <Components.TopBar titre="Luminosite" onPress={() => props.navigation.goBack()} />
      <View style={styles.scroll}>
        <View style={styles.flexcolumn}>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>{t('Luminosite.clair')}</Text>
            </View>
            <View
              style={{
                backgroundColor: checked == 'first' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="first"
                color={checked == 'first' ? '#FFFFFF' : null}
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('first')}
              />
            </View>
          </View>

          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>{t('Luminosite.sombre')}</Text>
            </View>

            <View
              style={{
                backgroundColor: checked == 'second' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                color={checked == 'second' ? '#FFFFFF' : null}
                value="second"
                status={checked === 'second' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('second')}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Luminosite;
