import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import * as Components from '../../../Components/index';
import Mentionslegalesicon from '../../../Assets/Images/Parametres/mentionslegalesIcon';
import NoterIcon from '../../../Assets/Images/Parametres/noteIcon';
import LuminositeIcon from '../../../Assets/Images/Parametres/luminositeIcon';
import {useTranslation} from 'react-i18next';
import {styles} from './Styles/Parametres';
import MentionsLegales from './MentionsLegales';
import Slider from '../../../Assets/Images/Parametres/sliders'

const {width, height} = Dimensions.get('screen');

const Parametres = props => {

  const {t} = useTranslation();

  return (
    <View style={styles.container}>
      <Components.TopBar titre="Paramètres" />
      <View style={styles.scroll}>
        <View style={styles.parametres}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('DescThematic')}>
            <View style={styles.flex}>
              <Image style={styles.svg2} source={require('../../../Assets/Images/Parametres/thematiquede.png')} />
              <Text style={styles.text}>{t('ParametresPage.descriptionthematique')}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => props.navigation.navigate('Langue')}>
            <View style={styles.flex}>
                <Image style={styles.svg2} source={require('../../../Assets/Images/Parametres/langues.png')} />
              <Text style={styles.text}>{t('ParametresPage.langues')}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate('Sliders2')
            }>
            <View style={styles.flex}>
              <Image style={styles.svg3} source={require('../../../Assets/Images/Parametres/Slide.png')} />
              <Text style={styles.text}>
                {t('ParametresPage.sliders')}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate('PolitiqueConfidentialite')
            }>
            <View style={styles.flex}>
              <Image style={styles.svg2} source={require('../../../Assets/Images/Parametres/cadena.png')} />
              <Text style={styles.text}>
                {t('ParametresPage.politiqueConfidentialite')}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('ConditionUtilisation')}>
            <View style={styles.flex}>
              <Image style={styles.svg2} source={require('../../../Assets/Images/Parametres/book.png')} />
              <Text style={styles.text}>
                {t('ParametresPage.conditionsUtilisation')}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('MentionsLegales')}>
            <View style={styles.flex}>
              <Image style={styles.svg4} source={require('../../../Assets/Images/Parametres/I.png')} />
              <Text style={styles.text}>
                {t('ParametresPage.mentionLegales')}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
/*<TouchableOpacity onPress={() => props.navigation.navigate('Voix')}>
            <View style={styles.flex}>
              <View style={styles.svg}>
                <VoixIcon width={width} height={'100%'} />
              </View>
              <Text style={styles.text}>{t('ParametresPage.voix')}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.flex}>
              <View style={styles.svg}>
                <NoterIcon width={width} height={'100%'} />
              </View>
              <Text style={styles.text}>
                {t('ParametresPage.noteApplication')}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('Luminosite')}>
            <View style={styles.flex}>
              <View style={styles.svg}>
                <LuminositeIcon width={width} height={'100%'} />
              </View>
              <Text style={styles.text}>{t('ParametresPage.luminosite')}</Text>
            </View>
          </TouchableOpacity>
          */
export default Parametres;
