import React, {useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import * as Components from '../../../Components/index';
import {useTranslation} from 'react-i18next';
import {RadioButton} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {styles} from './Styles/Langue';
import {useDispatch, useSelector} from 'react-redux';
import {
  getAllLieux,
  getLieuxbyid,
  updatelangue,
  pagecounter,
} from '../../../actions';

const {width, height} = Dimensions.get('screen');

// Composant principal
const Langue = (props) => {
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();
  const [language, setLanguage] = React.useState('');
  const lieuxe = useSelector(state => state.lieux);
  const page = 0;

  // const test = AsyncStorage.getItem('languagedsds')
  AsyncStorage.getItem('language').then(value => {
    if (value !== null) {
      setLanguage(value);
    } else {
      console.log('pasdevaluer');
    }
  });

  const handleChange4 = () => {
    console.log(i18n.changeLanguage('ar'));
    let language = 'ar';
    AsyncStorage.setItem('language', 'ar');
    i18n.changeLanguage('ar');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange5 = () => {
    //  console.log(i18n.changeLanguage('fr'))
    let language = 'bn';
    AsyncStorage.setItem('language', 'bn');
    i18n.changeLanguage('bn');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange2 = () => {
    // console.log(i18n.changeLanguage('en'))
    let language = 'en';
    AsyncStorage.setItem('language', 'en');
    i18n.changeLanguage('en');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange1 = () => {
    //  console.log(i18n.changeLanguage('fr'))
    let language = 'fr';
    AsyncStorage.setItem('language', 'fr');
    i18n.changeLanguage('fr');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange7 = () => {
    // console.log(i18n.changeLanguage('en'))
    let language = 'ru';
    AsyncStorage.setItem('language', 'ru');
    i18n.changeLanguage('ru');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange3 = () => {
    let language = 'zh';
    AsyncStorage.setItem('language', 'zh');
    i18n.changeLanguage('zh');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange8 = () => {
    let language = 'ur';
    AsyncStorage.setItem('language', 'ur');
    i18n.changeLanguage('ur');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };
  const handleChange9 = () => {
    let language = 'af';
    AsyncStorage.setItem('language', 'af');
    i18n.changeLanguage('af');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange10 = () => {
    let language = 'am';
    AsyncStorage.setItem('language', 'am');
    i18n.changeLanguage('am');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange11 = () => {
    let language = 'az';
    AsyncStorage.setItem('language', 'az');
    i18n.changeLanguage('az');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange12 = () => {
    let language = 'bg';
    AsyncStorage.setItem('language', 'bg');
    i18n.changeLanguage('bg');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange13 = () => {
    let language = 'bs';
    AsyncStorage.setItem('language', 'bs');
    i18n.changeLanguage('bs');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange14 = () => {
    let language = 'ca';
    AsyncStorage.setItem('language', 'ca');
    i18n.changeLanguage('ca');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange15 = () => {
    let language = 'ceb';
    AsyncStorage.setItem('language', 'ceb');
    i18n.changeLanguage('ceb');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange16 = () => {
    let language = 'de';
    AsyncStorage.setItem('language', 'de');
    i18n.changeLanguage('de');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange21 = () => {
    // console.log(i18n.changeLanguage('en'))
    let language = 'eu';
    AsyncStorage.setItem('language', 'eu');
    i18n.changeLanguage('eu');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange17 = () => {
    let language = 'hi';
    AsyncStorage.setItem('language', 'hi');
    i18n.changeLanguage('hi');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange18 = () => {
    let language = 'hy';
    AsyncStorage.setItem('language', 'hy');
    i18n.changeLanguage('hy');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange19 = () => {
    let language = 'ny';
    AsyncStorage.setItem('language', 'ny');
    i18n.changeLanguage('ny');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange20 = () => {
    let language = 'sq';
    AsyncStorage.setItem('language', 'sq');
    i18n.changeLanguage('sq');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange22 = () => {
    let language = 'co';
    AsyncStorage.setItem('language', 'co');
    i18n.changeLanguage('co');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange23 = () => {
    let language = 'cs';
    AsyncStorage.setItem('language', 'cs');
    i18n.changeLanguage('cs');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange24 = () => {
    let language = 'da';
    AsyncStorage.setItem('language', 'da');
    i18n.changeLanguage('da');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange25 = () => {
    let language = 'el';
    AsyncStorage.setItem('language', 'el');
    i18n.changeLanguage('el');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange26 = () => {
    let language = 'eo';
    AsyncStorage.setItem('language', 'eo');
    i18n.changeLanguage('eo');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange27 = () => {
    let language = 'et';
    AsyncStorage.setItem('language', 'et');
    i18n.changeLanguage('et');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange28 = () => {
    let language = 'fi';
    AsyncStorage.setItem('language', 'fi');
    i18n.changeLanguage('fi');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange29 = () => {
    let language = 'fy';
    AsyncStorage.setItem('language', 'fy');
    i18n.changeLanguage('fy');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange30 = () => {
    let language = 'gl';
    AsyncStorage.setItem('language', 'gl');
    i18n.changeLanguage('gl');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange31 = () => {
    let language = 'gu';
    AsyncStorage.setItem('language', 'gu');
    i18n.changeLanguage('gu');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange32 = () => {
    let language = 'hr';
    AsyncStorage.setItem('language', 'hr');
    i18n.changeLanguage('hr');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange33 = () => {
    let language = 'ka';
    AsyncStorage.setItem('language', 'ka');
    i18n.changeLanguage('ka');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange34 = () => {
    let language = 'nl';
    AsyncStorage.setItem('language', 'nl');
    i18n.changeLanguage('nl');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange35 = () => {
    let language = 'tl';
    AsyncStorage.setItem('language', 'tl');
    i18n.changeLanguage('tl');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange36 = () => {
    let language = 'fa';
    AsyncStorage.setItem('language', 'fa');
    i18n.changeLanguage('fa');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange37 = () => {
    let language = 'gd';
    AsyncStorage.setItem('language', 'gd');
    i18n.changeLanguage('gd');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange38 = () => {
    let language = 'ku';
    AsyncStorage.setItem('language', 'ku');
    i18n.changeLanguage('ku');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange39 = () => {
    let language = 'ky';
    AsyncStorage.setItem('language', 'ky');
    i18n.changeLanguage('ky');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange40 = () => {
    let language = 'la';
    AsyncStorage.setItem('language', 'la');
    i18n.changeLanguage('la');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange41 = () => {
    let language = 'lb';
    AsyncStorage.setItem('language', 'lb');
    i18n.changeLanguage('lb');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange42 = () => {
    let language = 'lo';
    AsyncStorage.setItem('language', 'lo');
    i18n.changeLanguage('lo');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange43 = () => {
    let language = 'lt';
    AsyncStorage.setItem('language', 'lt');
    i18n.changeLanguage('lt');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange44 = () => {
    let language = 'lv';
    AsyncStorage.setItem('language', 'lv');
    i18n.changeLanguage('lv');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange45 = () => {
    let language = 'mg';
    AsyncStorage.setItem('language', 'mg');
    i18n.changeLanguage('mg');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange46 = () => {
    let language = 'mi';
    AsyncStorage.setItem('language', 'mi');
    i18n.changeLanguage('mi');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange47 = () => {
    let language = 'mk';
    AsyncStorage.setItem('language', 'mk');
    i18n.changeLanguage('mk');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange48 = () => {
    let language = 'ml';
    AsyncStorage.setItem('language', 'ml');
    i18n.changeLanguage('ml');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange49 = () => {
    let language = 'mn';
    AsyncStorage.setItem('language', 'mn');
    i18n.changeLanguage('mn');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange50 = () => {
    let language = 'ms';
    AsyncStorage.setItem('language', 'ms');
    i18n.changeLanguage('ms');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange51 = () => {
    let language = 'mt';
    AsyncStorage.setItem('language', 'mt');
    i18n.changeLanguage('mt');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange52 = () => {
    let language = 'my';
    AsyncStorage.setItem('language', 'my');
    i18n.changeLanguage('my');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange53 = () => {
    let language = 'ne';
    AsyncStorage.setItem('language', 'ne');
    i18n.changeLanguage('ne');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange54 = () => {
    let language = 'no';
    AsyncStorage.setItem('language', 'no');
    i18n.changeLanguage('no');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange55 = () => {
    let language = 'pl';
    AsyncStorage.setItem('language', 'pl');
    i18n.changeLanguage('pl');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange56 = () => {
    let language = 'ps';
    AsyncStorage.setItem('language', 'ps');
    i18n.changeLanguage('ps');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange57 = () => {
    let language = 'ro';
    AsyncStorage.setItem('language', 'ro');
    i18n.changeLanguage('ro');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange58 = () => {
    let language = 'sm';
    AsyncStorage.setItem('language', 'sm');
    i18n.changeLanguage('sm');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange59 = () => {
    let language = 'cy';
    AsyncStorage.setItem('language', 'cy');
    i18n.changeLanguage('cy');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange60 = () => {
    let language = 'ga';
    AsyncStorage.setItem('language', 'ga');
    i18n.changeLanguage('ga');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange62 = () => {
    let language = 'ha';
    AsyncStorage.setItem('language', 'ha');
    i18n.changeLanguage('ha');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange63 = () => {
    let language = 'haw';
    AsyncStorage.setItem('language', 'haw');
    i18n.changeLanguage('haw');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange65 = () => {
    let language = 'ht';
    AsyncStorage.setItem('language', 'ht');
    i18n.changeLanguage('ht');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange66 = () => {
    let language = 'hu';
    AsyncStorage.setItem('language', 'hu');
    i18n.changeLanguage('hu');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange67 = () => {
    let language = 'id';
    AsyncStorage.setItem('language', 'id');
    i18n.changeLanguage('id');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange68 = () => {
    let language = 'ig';
    AsyncStorage.setItem('language', 'ig');
    i18n.changeLanguage('ig');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange69 = () => {
    let language = 'is';
    AsyncStorage.setItem('language', 'is');
    i18n.changeLanguage('is');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange70 = () => {
    let language = 'iw';
    AsyncStorage.setItem('language', 'iw');
    i18n.changeLanguage('iw');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange71 = () => {
    let language = 'ja';
    AsyncStorage.setItem('language', 'ja');
    i18n.changeLanguage('ja');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange73 = () => {
    let language = 'kk';
    AsyncStorage.setItem('language', 'kk');
    i18n.changeLanguage('kk');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange74 = () => {
    let language = 'es';
    AsyncStorage.setItem('language', 'es');
    i18n.changeLanguage('es');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange75 = () => {
    let language = 'it';
    AsyncStorage.setItem('language', 'it');
    i18n.changeLanguage('it');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange76 = () => {
    let language = 'km';
    AsyncStorage.setItem('language', 'km');
    i18n.changeLanguage('km');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange77 = () => {
    let language = 'kn';
    AsyncStorage.setItem('language', 'kn');
    i18n.changeLanguage('kn');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange78 = () => {
    let language = 'pt';
    AsyncStorage.setItem('language', 'pt');
    i18n.changeLanguage('pt');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange79 = () => {
    let language = 'sd';
    AsyncStorage.setItem('language', 'sd');
    i18n.changeLanguage('sd');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange81 = () => {
    let language = 'si';
    AsyncStorage.setItem('language', 'si');
    i18n.changeLanguage('si');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange82 = () => {
    let language = 'sk';
    AsyncStorage.setItem('language', 'sk');
    i18n.changeLanguage('sk');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange83 = () => {
    let language = 'sl';
    AsyncStorage.setItem('language', 'sl');
    i18n.changeLanguage('sl');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange84 = () => {
    let language = 'sn';
    AsyncStorage.setItem('language', 'sn');
    i18n.changeLanguage('sn');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange85 = () => {
    let language = 'so';
    AsyncStorage.setItem('language', 'so');
    i18n.changeLanguage('so');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange86 = () => {
    let language = 'sr';
    AsyncStorage.setItem('language', 'sr');
    i18n.changeLanguage('sr');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange87 = () => {
    let language = 'st';
    AsyncStorage.setItem('language', 'st');
    i18n.changeLanguage('st');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange88 = () => {
    let language = 'su';
    AsyncStorage.setItem('language', 'su');
    i18n.changeLanguage('su');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange89 = () => {
    let language = 'sv';
    AsyncStorage.setItem('language', 'sv');
    i18n.changeLanguage('sv');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange90 = () => {
    let language = 'sw';
    AsyncStorage.setItem('language', 'sw');
    i18n.changeLanguage('sw');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange91 = () => {
    let language = 'ta';
    AsyncStorage.setItem('language', 'ta');
    i18n.changeLanguage('ta');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange92 = () => {
    let language = 'te';
    AsyncStorage.setItem('language', 'te');
    i18n.changeLanguage('te');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange93 = () => {
    let language = 'tg';
    AsyncStorage.setItem('language', 'tg');
    i18n.changeLanguage('tg');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange94 = () => {
    let language = 'th';
    AsyncStorage.setItem('language', 'th');
    i18n.changeLanguage('th');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange95 = () => {
    let language = 'tr';
    AsyncStorage.setItem('language', 'tr');
    i18n.changeLanguage('tr');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange98 = () => {
    let language = 'uk';
    AsyncStorage.setItem('language', 'uk');
    i18n.changeLanguage('uk');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange99 = () => {
    let language = 'uz';
    AsyncStorage.setItem('language', 'uz');
    i18n.changeLanguage('uz');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange100 = () => {
    let language = 'vi';
    AsyncStorage.setItem('language', 'vi');
    i18n.changeLanguage('vi');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange101 = () => {
    let language = 'xh';
    AsyncStorage.setItem('language', 'xh');
    i18n.changeLanguage('xh');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange102 = () => {
    let language = 'yi';
    AsyncStorage.setItem('language', 'yi');
    i18n.changeLanguage('yi');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange103 = () => {
    let language = 'yo';
    AsyncStorage.setItem('language', 'yo');
    i18n.changeLanguage('yo');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  const handleChange104 = () => {
    let language = 'zu';
    AsyncStorage.setItem('language', 'zu');
    i18n.changeLanguage('zu');
    dispatch(updatelangue(language));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page,
        lieuxe.longitude,
        lieuxe.latitude,
        language,
        lieuxe.celebrit,
      ),
    );
  };

  return (
    <View style={styles.container}>
      <Components.TopBar
        titre="Langue"
        onPress={() => props.navigation.goBack()}
      />
      <View style={styles.scroll}>
        <ScrollView style={styles.flexcolumn}>
          <View style={styles.itemStyle0}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Afrikaans</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'af' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="af"
                color={language == 'af' ? '#FFFFFF' : null}
                status={language === 'af' ? 'checked' : 'unchecked'}
                onPress={() => handleChange9()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>አማርኛ</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'am' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="am"
                color={language == 'am' ? '#FFFFFF' : null}
                status={language === 'am' ? 'checked' : 'unchecked'}
                onPress={() => handleChange10()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>العربية</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ar' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ar"
                color={language == 'ar' ? '#FFFFFF' : null}
                status={language === 'ar' ? 'checked' : 'unchecked'}
                onPress={() => handleChange4()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Azərbaycanca</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'az' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="az"
                color={language == 'az' ? '#FFFFFF' : null}
                status={language === 'az' ? 'checked' : 'unchecked'}
                onPress={() => handleChange11()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Български</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'bg' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="bg"
                color={language == 'bg' ? '#FFFFFF' : null}
                status={language === 'bg' ? 'checked' : 'unchecked'}
                onPress={() => handleChange12()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>বাংলা</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'bn' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="bn"
                color={language == 'bn' ? '#FFFFFF' : null}
                status={language === 'bn' ? 'checked' : 'unchecked'}
                onPress={() => handleChange5()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Bosanski</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'bs' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="bs"
                color={language == 'bs' ? '#FFFFFF' : null}
                status={language === 'bs' ? 'checked' : 'unchecked'}
                onPress={() => handleChange13()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Català</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ca' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ca"
                color={language == 'ca' ? '#FFFFFF' : null}
                status={language === 'ca' ? 'checked' : 'unchecked'}
                onPress={() => handleChange14()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Cebuano</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ceb' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ceb"
                color={language == 'ceb' ? '#FFFFFF' : null}
                status={language === 'ceb' ? 'checked' : 'unchecked'}
                onPress={() => handleChange15()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Corsu</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'co' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="co"
                color={language == 'co' ? '#FFFFFF' : null}
                status={language === 'co' ? 'checked' : 'unchecked'}
                onPress={() => handleChange22()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Čeština</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'cs' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="cs"
                color={language == 'cs' ? '#FFFFFF' : null}
                status={language === 'cs' ? 'checked' : 'unchecked'}
                onPress={() => handleChange23()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Cymraeg</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'cy' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="cy"
                color={language == 'cy' ? '#FFFFFF' : null}
                status={language === 'cy' ? 'checked' : 'unchecked'}
                onPress={() => handleChange59()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Dansk</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'da' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="da"
                color={language == 'da' ? '#FFFFFF' : null}
                status={language === 'da' ? 'checked' : 'unchecked'}
                onPress={() => handleChange24()}
              />
            </View>
          </View>

          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Deutsch</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'de' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="de"
                color={language == 'de' ? '#FFFFFF' : null}
                status={language === 'de' ? 'checked' : 'unchecked'}
                onPress={() => handleChange16()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Ελληνικά</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'el' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                color={language == 'el' ? '#FFFFFF' : null}
                value="el"
                status={language === 'el' ? 'checked' : 'unchecked'}
                onPress={() => handleChange25()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>English</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'en' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                color={language == 'en' ? '#FFFFFF' : null}
                value="en"
                status={language === 'en' ? 'checked' : 'unchecked'}
                onPress={() => handleChange2()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Esperanto</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'eo' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                color={language == 'eo' ? '#FFFFFF' : null}
                value="eo"
                status={language === 'eo' ? 'checked' : 'unchecked'}
                onPress={() => handleChange26()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Español</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'es' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                color={language == 'es' ? '#FFFFFF' : null}
                value="es"
                status={language === 'es' ? 'checked' : 'unchecked'}
                onPress={() => handleChange74()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Eesti</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'et' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                color={language == 'et' ? '#FFFFFF' : null}
                value="et"
                status={language === 'et' ? 'checked' : 'unchecked'}
                onPress={() => handleChange27()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Euskara</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'eu' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                color={language == 'eu' ? '#FFFFFF' : null}
                value="eu"
                status={language === 'eu' ? 'checked' : 'unchecked'}
                onPress={() => handleChange21()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>فارسی</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'fa' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="fa"
                color={language == 'fa' ? '#FFFFFF' : null}
                status={language === 'fa' ? 'checked' : 'unchecked'}
                onPress={() => handleChange36()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Suomi</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'fi' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="fi"
                color={language == 'fi' ? '#FFFFFF' : null}
                status={language === 'fi' ? 'checked' : 'unchecked'}
                onPress={() => handleChange28()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Français</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'fr' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="fr"
                color={language == 'fr' ? '#FFFFFF' : null}
                status={language === 'fr' ? 'checked' : 'unchecked'}
                onPress={() => handleChange1()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Frysk</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'fy' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="fy"
                color={language == 'fy' ? '#FFFFFF' : null}
                status={language === 'fy' ? 'checked' : 'unchecked'}
                onPress={() => handleChange29()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Gaeilge</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ga' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ga"
                color={language == 'ga' ? '#FFFFFF' : null}
                status={language === 'ga' ? 'checked' : 'unchecked'}
                onPress={() => handleChange60()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Gàidhlig</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'gd' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="gd"
                color={language == 'gd' ? '#FFFFFF' : null}
                status={language === 'gd' ? 'checked' : 'unchecked'}
                onPress={() => handleChange37()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Galego</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'gl' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="gl"
                color={language == 'gl' ? '#FFFFFF' : null}
                status={language === 'gl' ? 'checked' : 'unchecked'}
                onPress={() => handleChange30()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>ગુજરાતી</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'gu' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="gu"
                color={language == 'gu' ? '#FFFFFF' : null}
                status={language === 'gu' ? 'checked' : 'unchecked'}
                onPress={() => handleChange31()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Hausa</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ha' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ha"
                color={language == 'ha' ? '#FFFFFF' : null}
                status={language === 'ha' ? 'checked' : 'unchecked'}
                onPress={() => handleChange62()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Hawaiʻi</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'haw' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="haw"
                color={language == 'haw' ? '#FFFFFF' : null}
                status={language === 'haw' ? 'checked' : 'unchecked'}
                onPress={() => handleChange63()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>हिन्दी</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'hi' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="hi"
                color={language == 'hi' ? '#FFFFFF' : null}
                status={language === 'hi' ? 'checked' : 'unchecked'}
                onPress={() => handleChange17()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Hrvatski</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'hr' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="hr"
                color={language == 'hr' ? '#FFFFFF' : null}
                status={language === 'hr' ? 'checked' : 'unchecked'}
                onPress={() => handleChange32()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Kreyòl ayisyen</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ht' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ht"
                color={language == 'ht' ? '#FFFFFF' : null}
                status={language === 'ht' ? 'checked' : 'unchecked'}
                onPress={() => handleChange65()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Magyar</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'hu' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="hu"
                color={language == 'hu' ? '#FFFFFF' : null}
                status={language === 'hu' ? 'checked' : 'unchecked'}
                onPress={() => handleChange66()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Հայերեն</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'hy' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="hy"
                color={language == 'hy' ? '#FFFFFF' : null}
                status={language === 'hy' ? 'checked' : 'unchecked'}
                onPress={() => handleChange18()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Bahasa Indonesia</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'id' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="id"
                color={language == 'id' ? '#FFFFFF' : null}
                status={language === 'id' ? 'checked' : 'unchecked'}
                onPress={() => handleChange67()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Igbo</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ig' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ig"
                color={language == 'ig' ? '#FFFFFF' : null}
                status={language === 'ig' ? 'checked' : 'unchecked'}
                onPress={() => handleChange68()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Icelandic</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'is' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="is"
                color={language == 'is' ? '#FFFFFF' : null}
                status={language === 'is' ? 'checked' : 'unchecked'}
                onPress={() => handleChange69()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Italiano</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'it' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="it"
                color={language == 'it' ? '#FFFFFF' : null}
                status={language === 'it' ? 'checked' : 'unchecked'}
                onPress={() => handleChange75()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Hebrew</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'iw' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="iw"
                color={language == 'iw' ? '#FFFFFF' : null}
                status={language === 'iw' ? 'checked' : 'unchecked'}
                onPress={() => handleChange70()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>日本語</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ja' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ja"
                color={language == 'ja' ? '#FFFFFF' : null}
                status={language === 'ja' ? 'checked' : 'unchecked'}
                onPress={() => handleChange71()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>ქართული</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ka' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ka"
                color={language == 'ka' ? '#FFFFFF' : null}
                status={language === 'ka' ? 'checked' : 'unchecked'}
                onPress={() => handleChange33()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Қазақша</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'kk' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="kk"
                color={language == 'kk' ? '#FFFFFF' : null}
                status={language === 'kk' ? 'checked' : 'unchecked'}
                onPress={() => handleChange73()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>ភាសាខ្មែរ</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'km' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="km"
                color={language == 'km' ? '#FFFFFF' : null}
                status={language === 'km' ? 'checked' : 'unchecked'}
                onPress={() => handleChange76()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>ಕನ್ನಡ</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'kn' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="kn"
                color={language == 'kn' ? '#FFFFFF' : null}
                status={language === 'kn' ? 'checked' : 'unchecked'}
                onPress={() => handleChange77()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>کوردی</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ku' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ku"
                color={language == 'ku' ? '#FFFFFF' : null}
                status={language === 'ku' ? 'checked' : 'unchecked'}
                onPress={() => handleChange38()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Кыргызча</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ky' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ky"
                color={language == 'ky' ? '#FFFFFF' : null}
                status={language === 'ky' ? 'checked' : 'unchecked'}
                onPress={() => handleChange39()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Latina</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'la' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="la"
                color={language == 'la' ? '#FFFFFF' : null}
                status={language === 'la' ? 'checked' : 'unchecked'}
                onPress={() => handleChange40()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Lëtzebuergesch</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'lb' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="lb"
                color={language == 'lb' ? '#FFFFFF' : null}
                status={language === 'lb' ? 'checked' : 'unchecked'}
                onPress={() => handleChange41()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>ລາວ</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'lo' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="lo"
                color={language == 'lo' ? '#FFFFFF' : null}
                status={language === 'lo' ? 'checked' : 'unchecked'}
                onPress={() => handleChange42()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Lietuvių</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'lt' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="lt"
                color={language == 'lt' ? '#FFFFFF' : null}
                status={language === 'lt' ? 'checked' : 'unchecked'}
                onPress={() => handleChange43()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Latviešu</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'lv' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="lv"
                color={language == 'lv' ? '#FFFFFF' : null}
                status={language === 'lv' ? 'checked' : 'unchecked'}
                onPress={() => handleChange44()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Malagasy</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'mg' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="mg"
                color={language == 'mg' ? '#FFFFFF' : null}
                status={language === 'mg' ? 'checked' : 'unchecked'}
                onPress={() => handleChange45()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Māori</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'mi' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="mi"
                color={language == 'mi' ? '#FFFFFF' : null}
                status={language === 'mi' ? 'checked' : 'unchecked'}
                onPress={() => handleChange46()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Македонски</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'mk' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="mk"
                color={language == 'mk' ? '#FFFFFF' : null}
                status={language === 'mk' ? 'checked' : 'unchecked'}
                onPress={() => handleChange47()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>മലയാളം</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ml' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ml"
                color={language == 'ml' ? '#FFFFFF' : null}
                status={language === 'ml' ? 'checked' : 'unchecked'}
                onPress={() => handleChange48()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Монгол</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'mn' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="mn"
                color={language == 'mn' ? '#FFFFFF' : null}
                status={language === 'mn' ? 'checked' : 'unchecked'}
                onPress={() => handleChange49()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Bahasa Melayu</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ms' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ms"
                color={language == 'ms' ? '#FFFFFF' : null}
                status={language === 'ms' ? 'checked' : 'unchecked'}
                onPress={() => handleChange50()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Malti</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'mt' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="mt"
                color={language == 'mt' ? '#FFFFFF' : null}
                status={language === 'mt' ? 'checked' : 'unchecked'}
                onPress={() => handleChange51()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>မြန်မာဘာသာ</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'my' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="my"
                color={language == 'my' ? '#FFFFFF' : null}
                status={language === 'my' ? 'checked' : 'unchecked'}
                onPress={() => handleChange52()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>नेपाली</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ne' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ne"
                color={language == 'ne' ? '#FFFFFF' : null}
                status={language === 'ne' ? 'checked' : 'unchecked'}
                onPress={() => handleChange53()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Nederlands</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'nl' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="nl"
                color={language == 'nl' ? '#FFFFFF' : null}
                status={language === 'nl' ? 'checked' : 'unchecked'}
                onPress={() => handleChange34()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Norsk bokmål</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'no' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="no"
                color={language == 'no' ? '#FFFFFF' : null}
                status={language === 'no' ? 'checked' : 'unchecked'}
                onPress={() => handleChange54()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Chi-Chewa</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ny' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ny"
                color={language == 'ny' ? '#FFFFFF' : null}
                status={language === 'ny' ? 'checked' : 'unchecked'}
                onPress={() => handleChange19()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Polski</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'pl' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="pl"
                color={language == 'pl' ? '#FFFFFF' : null}
                status={language === 'pl' ? 'checked' : 'unchecked'}
                onPress={() => handleChange55()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Português</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'pt' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="pt"
                color={language == 'pt' ? '#FFFFFF' : null}
                status={language === 'pt' ? 'checked' : 'unchecked'}
                onPress={() => handleChange78()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>پښتو</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ps' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ps"
                color={language == 'ps' ? '#FFFFFF' : null}
                status={language === 'ps' ? 'checked' : 'unchecked'}
                onPress={() => handleChange56()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Română</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ro' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ro"
                color={language == 'ro' ? '#FFFFFF' : null}
                status={language === 'ro' ? 'checked' : 'unchecked'}
                onPress={() => handleChange57()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Русский</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ru' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ru"
                color={language == 'ru' ? '#FFFFFF' : null}
                status={language === 'ru' ? 'checked' : 'unchecked'}
                onPress={() => handleChange7()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>سنڌي</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'sd' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="sd"
                color={language == 'sd' ? '#FFFFFF' : null}
                status={language === 'sd' ? 'checked' : 'unchecked'}
                onPress={() => handleChange79()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>සිංහල</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'si' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="si"
                color={language == 'si' ? '#FFFFFF' : null}
                status={language === 'si' ? 'checked' : 'unchecked'}
                onPress={() => handleChange81()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Slovak</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'sk' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="sk"
                color={language == 'sk' ? '#FFFFFF' : null}
                status={language === 'sk' ? 'checked' : 'unchecked'}
                onPress={() => handleChange82()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Slovenian</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'sl' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="sl"
                color={language == 'sl' ? '#FFFFFF' : null}
                status={language === 'sl' ? 'checked' : 'unchecked'}
                onPress={() => handleChange83()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Gagana Samoa</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'sm' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="sm"
                color={language == 'sm' ? '#FFFFFF' : null}
                status={language === 'sm' ? 'checked' : 'unchecked'}
                onPress={() => handleChange58()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>ChiShona</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'sn' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="sn"
                color={language == 'sn' ? '#FFFFFF' : null}
                status={language === 'sn' ? 'checked' : 'unchecked'}
                onPress={() => handleChange84()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Soomaaliga</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'so' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="so"
                color={language == 'so' ? '#FFFFFF' : null}
                status={language === 'so' ? 'checked' : 'unchecked'}
                onPress={() => handleChange85()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Српски / srpski</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'sr' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="sr"
                color={language == 'sr' ? '#FFFFFF' : null}
                status={language === 'sr' ? 'checked' : 'unchecked'}
                onPress={() => handleChange86()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Sesotho</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'st' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="st"
                color={language == 'st' ? '#FFFFFF' : null}
                status={language === 'st' ? 'checked' : 'unchecked'}
                onPress={() => handleChange87()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Sundanese</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'su' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="su"
                color={language == 'su' ? '#FFFFFF' : null}
                status={language === 'su' ? 'checked' : 'unchecked'}
                onPress={() => handleChange88()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Swedish</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'sv' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="sv"
                color={language == 'sv' ? '#FFFFFF' : null}
                status={language === 'sv' ? 'checked' : 'unchecked'}
                onPress={() => handleChange89()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Shqip</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'sq' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="sq"
                color={language == 'sq' ? '#FFFFFF' : null}
                status={language === 'sq' ? 'checked' : 'unchecked'}
                onPress={() => handleChange20()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Kiswahili</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'sw' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="sw"
                color={language == 'sw' ? '#FFFFFF' : null}
                status={language === 'sw' ? 'checked' : 'unchecked'}
                onPress={() => handleChange90()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>தமிழ்</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ta' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ta"
                color={language == 'ta' ? '#FFFFFF' : null}
                status={language === 'ta' ? 'checked' : 'unchecked'}
                onPress={() => handleChange91()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>తెలుగు</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'te' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="te"
                color={language == 'te' ? '#FFFFFF' : null}
                status={language === 'te' ? 'checked' : 'unchecked'}
                onPress={() => handleChange92()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Тоҷикӣ</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'tg' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="tg"
                color={language == 'tg' ? '#FFFFFF' : null}
                status={language === 'tg' ? 'checked' : 'unchecked'}
                onPress={() => handleChange93()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>ไทย</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'th' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="th"
                color={language == 'th' ? '#FFFFFF' : null}
                status={language === 'th' ? 'checked' : 'unchecked'}
                onPress={() => handleChange94()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Tagalog</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'tl' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="tl"
                color={language == 'tl' ? '#FFFFFF' : null}
                status={language === 'tl' ? 'checked' : 'unchecked'}
                onPress={() => handleChange35()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Türkçe</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'tr' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="tr"
                color={language == 'tr' ? '#FFFFFF' : null}
                status={language === 'tr' ? 'checked' : 'unchecked'}
                onPress={() => handleChange95()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Українська</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'uk' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="uk"
                color={language == 'uk' ? '#FFFFFF' : null}
                status={language === 'uk' ? 'checked' : 'unchecked'}
                onPress={() => handleChange98()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>اردو</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'ur' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="ur"
                color={language == 'ur' ? '#FFFFFF' : null}
                status={language === 'ur' ? 'checked' : 'unchecked'}
                onPress={() => handleChange8()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Oʻzbekcha/ўзбекча</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'uz' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="uz"
                color={language == 'uz' ? '#FFFFFF' : null}
                status={language === 'uz' ? 'checked' : 'unchecked'}
                onPress={() => handleChange99()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Tiếng Việt</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'vi' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="vi"
                color={language == 'vi' ? '#FFFFFF' : null}
                status={language === 'vi' ? 'checked' : 'unchecked'}
                onPress={() => handleChange100()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>IsiXhosa</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'xh' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="xh"
                color={language == 'xh' ? '#FFFFFF' : null}
                status={language === 'xh' ? 'checked' : 'unchecked'}
                onPress={() => handleChange101()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Yiddish</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'yi' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="yi"
                color={language == 'yi' ? '#FFFFFF' : null}
                status={language === 'yi' ? 'checked' : 'unchecked'}
                onPress={() => handleChange102()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>Yorùbá</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'yo' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="yo"
                color={language == 'yo' ? '#FFFFFF' : null}
                status={language === 'yo' ? 'checked' : 'unchecked'}
                onPress={() => handleChange103()}
              />
            </View>
          </View>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>中文</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'zh' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="zh"
                color={language == 'zh' ? '#FFFFFF' : null}
                status={language === 'zh' ? 'checked' : 'unchecked'}
                onPress={() => handleChange3()}
              />
            </View>
          </View>
          <View style={styles.itemStyle2}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>IsiZulu</Text>
            </View>
            <View
              style={{
                backgroundColor: language == 'zu' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="zu"
                color={language == 'zu' ? '#FFFFFF' : null}
                status={language === 'zu' ? 'checked' : 'unchecked'}
                onPress={() => handleChange104()}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default Langue;
