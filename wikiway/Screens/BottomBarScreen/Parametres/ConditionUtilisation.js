import React, { useCallback } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  Linking,
  Button
} from 'react-native';
import * as Components from '../../../Components/index';
import {useTranslation} from 'react-i18next';
import {RadioButton} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {styles} from './Styles/ConditionUtilsation';

const gitserv = 'https://gitlab.com/wikiway/wikiway-api'
const gitclient = 'https://gitlab.com/wikiway/wikiway-mobile-application'

const wikiway = 'https://gitlab.com/wikiway/wikiway-mobile-application'
const wikiwaymaquette = 'https://fr.wikipedia.org/wiki/Projet:Wikiway/Maquette_du_12_mai_2021'

const OpenURLButton = ({ url, children }) => {
  const handlePress = useCallback(async () => {
    // Checking if the link is supported for links with custom URL scheme.
    const supported = await Linking.canOpenURL(url);

    if (supported) {
      // Opening the link with some app, if the URL scheme is "http" the web link should be opened
      // by some browser in the mobile
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  }, [url]);
  return <Button title={children} onPress={handlePress} />;
}

const ConditionUtilisation = props => {
  const {t} = useTranslation();
  return (
    <View style={styles.container}>
      <Components.TopBar
        titre="ConditionUtilisation"
        onPress={() => props.navigation.goBack()}
      />
      <ScrollView style={styles.scroll}>
        <Text style={styles.textModalitem}>
        {t('ParametreText.conditionutilisation.1partie')}
        {t('ParametreText.conditionutilisation.linkserv')}<OpenURLButton url={gitserv}>https://gitlab.com/wikiway/wikiway-api</OpenURLButton>
        {t('ParametreText.conditionutilisation.linkmobile')}<OpenURLButton url={gitclient}>https://gitlab.com/wikiway/wikiway-mobile-application</OpenURLButton>
        {t('ParametreText.conditionutilisation.2partie')}
        </Text>
          <View style={styles.url}>
            <OpenURLButton url={wikiway}>https://fr.wikipedia.org/wiki/Projet:Wikiway</OpenURLButton>
            <OpenURLButton url={wikiwaymaquette}>https://fr.wikipedia.org/wiki/Projet:Wikiway/Maquette_du_12_mai_2021</OpenURLButton>
          </View>
        <Text style={styles.textModalitem}>
        {t('ParametreText.conditionutilisation.3partie')}
        </Text>
      </ScrollView>
    </View>
  );
};

export default ConditionUtilisation;
