import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.background,
  },
  scroll: {
    marginTop: Platform.OS === 'ios' ? height / 7 : height / 12,
    marginBottom: Platform.OS === 'ios' ? height / 35 : height/ 110,
    paddingRight: height/34,
    paddingLeft: height/34
  },
  item: {
    marginVertical: 11.5,
    marginHorizontal: 7.5,
  },
  centerText: {
    alignSelf: 'center',
  },
  textModalitem: {
    fontSize: width / 20,
    fontFamily:'Poppins-Regular',
    justifyContent: 'space-between',
    textAlign: 'left',
  },
  textsub:{
    marginTop: 3.5,
    paddingLeft: width /20,
    fontSize: width / 25,
    fontFamily:'Poppins-Regular',
    justifyContent: 'space-between',
    textAlign: 'left',
  },

  parametres: {
    marginTop: height / 35,
    paddingLeft: width / 20,
  },
  flexcolumn: {
    display: 'flex',
    flexDirection: 'column',
    padding: width / 16,
  },
 
});