import {StyleSheet, Dimensions} from 'react-native';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import {COLORS} from '../../../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
   
    container: {
        flex: 1,
        backgroundColor: COLORS.background,
      },
      scroll: {
        marginTop: Platform.OS === 'ios' ? height / 7: responsiveHeight(8),
      },

      itemStyle0: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
      },
    
      itemStyle: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: responsiveHeight(3),
      },
      itemStyle2: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: responsiveHeight(3),
        paddingBottom: responsiveHeight(8)
      },
      centerText: {
        alignSelf: 'center',
      },
      textModalitem: {
        fontSize: width / 19,
        fontFamily:'Poppins-Regular'
      },
      flexcolumn: {
        display: 'flex',
        flexDirection: 'column',
        padding: responsiveHeight(4),
      },

});