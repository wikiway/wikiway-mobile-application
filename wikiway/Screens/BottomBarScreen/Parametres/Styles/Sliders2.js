import {StyleSheet, Dimensions, useWindowDimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';
import { responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
const width = Dimensions.get('screen').width;
const height = Dimensions.get('screen').height;

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.background,
        alignItems: 'center',
        padding: height/10,
        justifyContent: 'center',
      },
      
      // titleStyle: {
      //   padding: 10,
      //   textAlign: 'center',
      //   fontSize: 18,
      //   fontWeight: 'bold',
      // },
    
      // introTitleStyle: {
      //   fontSize: 60,
      //   color: 'black',
      //   textAlign: 'center',
      //   marginBottom: 16,
      //   fontWeight: 'bold',
      // },

      introImageStyle1: {
        width: responsiveHeight(30),
        height: responsiveHeight(25),
      },

      introImageStyle2: {
        width: responsiveHeight(17),
        height: responsiveHeight(20.5),
      },
    
      introTextStyle: {
        fontSize: responsiveHeight(49),
        color: COLORS.green,
        textAlign: 'left',
        paddingVertical: 30,
        fontFamily: 'Poppins-Regular'
      },
    
      introTextStyle2: {
        fontSize: responsiveHeight(2.1),
        color: COLORS.black,
        textAlign: 'left',
        paddingVertical: 20,
        fontFamily: 'Poppins-Regular'
      },
    
      introTextSuivantTermine: {
        fontSize: 18,
        color: COLORS.green,
        textAlign: 'center',
        fontFamily: 'Poppins-Regular'
      },
      
      introTextPasser: {
        fontSize: 18,
        color: COLORS.black,
        textAlign: 'center',
        fontFamily: 'Poppins-Regular'
      },
    
      box: {
        marginTop: height / 55,
      },
    
 
});