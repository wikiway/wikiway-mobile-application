import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';


const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
   
    container: {
        flex: 1,
        backgroundColor: COLORS.background ,
      },
      scroll: {
        marginTop: Platform.OS === 'ios' ? height / 8 : height / 12,
      },
      slideicon:{
        color: COLORS.black,
      },
      parametres: {
        marginTop: height / 35,
        paddingLeft: width / 20,
      },
    
      flex: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: height / 40,
      },

      svg2: {
        height: responsiveHeight(6),
        width: responsiveHeight(6),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        padding: 5,
        marginRight: width / 7.5,
        marginLeft: width / 50,
      },
      svg3: {
        height: responsiveHeight(5.4),
        width: responsiveHeight(4.8),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        padding: 5,
        marginRight: width / 6.8,
        marginLeft: width / 32,
      },
      svg4: {
        height: responsiveHeight(6),
        width: responsiveHeight(6),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        padding: 5,
        marginRight: width / 7.4,
        marginLeft: width / 48,
      },
    
      text: {
        fontSize: responsiveFontSize(2),
        fontFamily:'Poppins-Medium',
        color:COLORS.parametreTextColor
      },
      
});