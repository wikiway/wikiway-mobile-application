import {StyleSheet, Dimensions} from 'react-native';
import { responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import {COLORS} from '../../../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
   
    container: {
        flex: 1,
        backgroundColor: COLORS.background,
      },
      scroll: {
        marginTop: Platform.OS === 'ios' ? height / 7 : height / 12,
        marginBottom: Platform.OS === 'ios' ? height / 35 : height/ 110,
        paddingRight: height/34,
        paddingLeft: height/34
      },
    
      itemStyle: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: height / 30,
      },
      centerText: {
        alignSelf: 'center',
      },
      textModalitem: {
        fontSize: width / 25,
        fontFamily:'Poppins-Regular',
        justifyContent: 'space-between',
        textAlign: 'left',
      },
      flexcolumn: {
        display: 'flex',
        flexDirection: 'column',
        padding: width / 16,
      },
      url: {
        display: 'flex',
        justifyContent: 'space-between',
        textAlign: 'left'
      }
});