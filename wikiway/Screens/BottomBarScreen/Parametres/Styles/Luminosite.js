import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
   
    container: {
        flex: 1,
        backgroundColor: COLORS.background,
      },
      scroll: {
        marginTop: Platform.OS === 'ios' ? height / 7 : height / 12,
      },
      itemStyle: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: height / 30,
      },
      centerText: {
        alignSelf: 'center',
      },
      textModalitem: {
        fontSize: width / 19,
        fontFamily:'Poppins-Regular'
      },
      flexcolumn: {
        display: 'flex',
        flexDirection: 'column',
        padding: width / 16,
      },
      
});