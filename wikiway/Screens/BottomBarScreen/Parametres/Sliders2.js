import React from 'react';
import {COLORS} from '../../../Assets/theme/color';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  ActivityIndicator,
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import * as Components from '../../../Components/index'
import {useTranslation} from 'react-i18next';
import {styles} from './Styles/Sliders2';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';

const {width, height} = Dimensions.get('screen');

const Sliders2 = props => {
  
  const {t} = useTranslation();
  const slides = [
    {
      key: 's1',
      image: require('../../../Assets/Images/Slider/ImageDiapo1.png'),
      title: t('Slider.titleslider1'),
      text: t('Slider.slider1'),
      styles: styles.introImageStyle1,
      backgroundColor: COLORS.background,
    },
    {
      key: 's2',
      image: require('../../../Assets/Images/Slider/ImageDiapo2.png'),
      title: t('Slider.titleslider2'),
      text: t('Slider.slider2'),
      styles: styles.introImageStyle2,
      backgroundColor: COLORS.background,
    },
  ];

  const RenderNextButton = () => {
    return (
      <View style={styles.box}>
        <Text style={styles.introTextSuivantTermine}>{t('Slider.next')}</Text>
      </View>
    );
  };

  const RenderDoneButton = () => {
    return (
      <View style={styles.box}>
        <Text style={styles.introTextSuivantTermine}>{t('Slider.done')}</Text>
      </View>
    );
  };

  const onDone = () => {
    props.navigation.navigate('Parametres');
  }

  const renderItem = ({item}) => {
    return (
      <View
        style={{
          flex: 1,
          display: 'flex',
          backgroundColor: item.backgroundColor,
          alignItems: 'center',
          justifyContent: 'center',
          paddingTop: height / 10,
          paddingBottom: height / 10,
          position: 'relative',
        }}>

        <Components.TopBar titre="Slides2"
        onPress ={() => props.navigation.navigate('Parametres')} />

        <View
          style={{
            width: responsiveWidth(90),
            height: responsiveHeight(70),
            backgroundColor: COLORS.white,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 20,
            padding: 5,
          }}>

          {/* <Text style={styles.introTitleStyle}>{item.Grostitre}</Text> */}
          {/* <Text style={styles.introTitleStyle}>{item.title}</Text>  */}

          <Image style={item.styles} source={item.image}/>

          <Text style={styles.introTextStyle2}>
            <Text style={styles.introTextStyle2}>{item.title}</Text>
            {item.text}
          </Text>
        </View>
      </View>
      
      
    );
  };

  return (
    <AppIntroSlider
      data={slides}
      onDone={onDone}
      renderItem={renderItem}
      dotStyle={{backgroundColor: COLORS.darkgrey}}
      activeDotStyle={{backgroundColor: COLORS.green}}
      renderNextButton={RenderNextButton}
      renderDoneButton={RenderDoneButton}
    />
  );
};

export default Sliders2;
