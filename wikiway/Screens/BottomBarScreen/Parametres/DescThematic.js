import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  Linking,
  Button
} from 'react-native';
import * as Components from '../../../Components/index';
import { styles } from './Styles/DescThematic';

const {width, height} = Dimensions.get('screen');

const DescThematic = props => {
    const {t} = useTranslation()
    return(
        <View style={styles.container}>
            <Components.TopBar
                titre="Description des thematiques"
                onPress={() => props.navigation.goBack()}
            />
            <ScrollView style={styles.scroll}>
                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.1')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.1text')}</Text>
                </View>

                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.7')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.2text')}</Text>
                </View>

                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.3')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.3text')}</Text>
                </View>
               
                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.8')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.4text')}</Text>
                </View>
                
                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.5')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.5text')}</Text>
                </View>
               
                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.4')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.6text')}</Text>
                </View>
                
                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.2')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.7text')}</Text>
                </View>
                
                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.9')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.8text')}</Text>
                </View>
               
                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.10')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.9text')}</Text>
                </View>

                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.6')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.10text')}</Text>
                </View>

                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.11')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.11text')}</Text>
                </View>

                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.12')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.12text')}</Text>
                </View>

                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.14')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.14text')}</Text>
                </View>

                <View style={styles.item}>
                    <Text style={styles.textModalitem}>{t('ParametreText.descriptionThematics.15')}</Text>
                    <Text style={styles.textsub}>{t('ParametreText.descriptionThematics.15text')}</Text>
                </View>
                
            </ScrollView>
        </View>

        );


}
export default DescThematic;