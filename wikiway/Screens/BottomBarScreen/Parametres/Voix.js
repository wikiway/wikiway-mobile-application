/*import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import * as Components from '../../../Components/index'
import {useTranslation} from 'react-i18next';
import {RadioButton} from 'react-native-paper';

const {width, height} = Dimensions.get('screen');

const Voix = props => {
  const {t} = useTranslation();
  const [checked, setChecked] = React.useState('first');

  return (
    <View style={styles.container}>
      <Components.TopBar titre="Voix" onPress={() => props.navigation.goBack()} /> 
      <View style={styles.scroll}>
      <View style={styles.flexcolumn}>
          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>{t('Voix.homme')}</Text>
            </View>
            <View
              style={{
                backgroundColor: checked == 'first' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                value="first"
                color={ checked == 'first' ? '#FFFFFF' : null}
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('first')}
              />
            </View>
          </View>

          <View style={styles.itemStyle}>
            <View style={styles.centerText}>
              <Text style={styles.textModalitem}>{t('Voix.femme')}</Text>
            </View>

            <View
              style={{
                backgroundColor: checked == 'second' ? '#45C6BE' : '#C4C4C4',
                borderRadius: 20,
              }}>
              <RadioButton
                color={checked == 'second' ? '#FFFFFF' : null}
                value="second"
                status={checked === 'second' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('second')}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F1F2F6',
  },
  scroll: {
    marginTop: Platform.OS === 'ios' ? height / 8 : height / 12,
  },

  itemStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: height / 30,
  },
  centerText: {
    alignSelf: 'center',
  },
  textModalitem: {
    fontSize: width / 19,
    fontFamily:'Poppins-Regular'
  },
  flexcolumn: {
    display: 'flex',
    flexDirection: 'column',
    padding: width / 16,
  },

 
});

export default Voix;*/
