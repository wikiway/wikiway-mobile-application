import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import * as Components from '../../../Components/index';
import {useTranslation} from 'react-i18next';
import {RadioButton} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {styles} from './Styles/MentionsLegales';



const MentionsLegales = props => {
  const {t} = useTranslation();
  return (
    <View style={styles.container}>
      <Components.TopBar
        titre="MentionsLegales"
        onPress={() => props.navigation.goBack()}
      />
      <ScrollView style={styles.scroll}>
        <Text style={styles.textModalitem}>
        {t('ParametreText.mentionslegales')}
        </Text>
      </ScrollView>
    </View>
  );
};

export default MentionsLegales;
