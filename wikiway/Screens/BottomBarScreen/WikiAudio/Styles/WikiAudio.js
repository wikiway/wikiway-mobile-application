import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
   
    container: {
        flex: 1,
        backgroundColor: '#F1F2F6',
      
      },
      scroll: {
        marginTop: Platform.OS === 'ios' ? height / 8 : height / 12,
        marginBottom: height / 50,
      },
    
      containerPosition1: {
        width: width,
        height: height / 4,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'blue',
        marginTop: height / 12,
      },
    
      son: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: height / 10,
      },
    
      text1: {
        fontFamily:'Poppins-Bold',
        fontSize: height / 50,
      },
    
      barson: {
        marginTop: height / 15,
      },
    
      musicpanel: {
        // backgroundColor: 'red',
        width: width,
        marginTop: height / 10,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft:width/45,
        paddingRight:width/45,
        position: 'relative'
      },
    
      leftdiv: {
        width: width/4,
        height:height/25,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 4
        // backgroundColor: 'purple',
      },
    
      rightdiv: {
        width: width/4,
        height:height/25,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop:4
        // backgroundColor: 'orange',
      },
    
      flexDirections: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      },
    
      divbutton:{
        position: 'absolute',
        left: width/2.5,
        top:-(height/25)
      }
});