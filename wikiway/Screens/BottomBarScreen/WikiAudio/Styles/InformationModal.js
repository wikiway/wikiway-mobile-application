import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
   
    container: {
        flex: 1,
        backgroundColor: 'white',
      },
      globalview: {
        marginTop: Platform.OS === 'ios' ? height / 8 : height / 15,
        padding: width / 23,
      },
      text: {
        marginTop: height / 45,
        fontSize: height / 55,
      },
      text1: {
        fontSize: height / 65,
        marginTop: height / 35,
        marginBottom: height / 35,
      },
      marginput: {
        marginBottom: height / 35,
      },
      container2: {
        flex: 1,
      },
      contentcolor2: {
        backgroundColor: '#000000aa',
        // flex: 1,
        width: width,
        height: height / 3,
      },
      modals2: {
        backgroundColor: '#ffffff',
        flex: 1,
        display: 'flex',
        padding: 20,
      },
      transpa: {
        height: height / 3.3,
        width: width,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
      },
      divdeco: {
        display: 'flex',
        height: '100%',
      },
      textdeco: {
        fontSize: height / 55,
        fontFamily:'Poppins-Regular',
        textAlign: 'center',
      },
      logo: {
        height: height / 5,
        width: width,
        backgroundColor: 'red',
      },
      close: {
        height: height / 40,
        width: width / 10,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: width / 1.2,
        marginTop: height / 50,
      },
      center: {
        display: 'flex',
        flexDirection:'column',
        alignItems: 'center',
        justifyContent: 'center',
      },
    
      itemStyle: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: height / 30,
      },
      centerText: {
        alignSelf: 'center',
      },
      textModalitem: {
        fontSize: width / 19,
      },
      image: {
        width: width / 1.5,
        height: height / 9,
       
      },

});