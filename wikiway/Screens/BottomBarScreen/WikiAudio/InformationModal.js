import React, {useState} from 'react';
import {
  View,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  Modal,
  StyleSheet,
  Text,
  Image,
} from 'react-native';

const {width, height} = Dimensions.get('screen');

import CloseIcon from '../../../Assets/Images/Modal/Close.svg';
import * as Components from '../../../Components/index'
import {RadioButton} from 'react-native-paper';
import AudioIcon from '../../../Assets/Images/TopBar/audio.svg';
import {useTranslation} from 'react-i18next'
import {styles} from './Styles/InformationModal';



const InformationModal = props => {
  const {t} = useTranslation();
  const [checked, setChecked] = React.useState('first');

  const [valeur, setValeur] = useState(0);
  const ExitModal = () => {
    setValeur(valeur + 1);
  };

  return (
    <View>
      <View style={styles.container}>
        <Modal transparent={true} visible={props.value > valeur ? true : false}>
          <TouchableOpacity
            style={styles.transpa}
            onPress={() => ExitModal()}></TouchableOpacity>
          <View style={styles.contentcolor2}>
            <View style={styles.modals2}>
              <View style={styles.divdeco}>
                <TouchableOpacity
                  style={styles.close}
                  onPress={() => ExitModal()}>
                  <CloseIcon width={width / 13} height={height / 23} />
                </TouchableOpacity>
                <View style={styles.center}>
                  {/* <View style={styles.logo}>
                      <AudioIcon width={'100%'} height={'100%'} />
                   </View> */}
                  <Image
                    source={require('../../../Assets/Images/TopBar/logo.png')}
                    style={styles.image}
                  />
                  <Text style={styles.textdeco}>
                  {t('WikiVoiceModal.text')}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <Components.NextButton text={t('WikiVoiceModal.confirmebutton')} onPress={() => ExitModal()} />
          <TouchableOpacity
            style={styles.transpa}
            onPress={() => ExitModal()}></TouchableOpacity>
        </Modal>
      </View>
    </View>
  );
};



export default InformationModal;
