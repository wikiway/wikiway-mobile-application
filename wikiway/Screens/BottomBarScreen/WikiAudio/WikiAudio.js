import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import * as Components from '../../../Components/index'
import SongbarIcon from '../../../Assets/Images/WikiAudio/Songbar';
import {styles} from './Styles/WikiAudio'
import TrackPlayer from '../../../Components/TrackPlayer';


const {width, height} = Dimensions.get('screen');

const WikiAudio = props => {
  return (
    <View style={styles.container}>
      <Components.TopBar
        titre="wikiaudio"
        onPress1={() => props.navigation.navigate('Form')}
        onPress2={() => console.log('information')}
      />
      <View style={styles.scroll}>
        <View style={styles.containerPosition1}>
          <Image
            source={require('../../../Assets/Images/DetailCard/card1.png')}
            style={styles.image}
          />
        </View>
        <View style={styles.son}>
          <Text style={styles.text1}>Avenue des champs Elysées</Text>
          <View style={styles.barson}>
            <SongbarIcon width={width / 1.2} height={height / 25} />
          </View>
          <Components.TrackPlayer
           onPress1={() => console.log('1')} 
           onPress2={() => console.log('2')}
           onPress3={() => console.log('3')}
           onPress4={() => console.log('4')}
           onPress5={() => console.log('5')}
           />
        </View>
      </View>
    </View>
  );
};



export default WikiAudio;
