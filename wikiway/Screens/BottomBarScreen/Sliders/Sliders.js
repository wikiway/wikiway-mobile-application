import React, {useState, useEffect} from 'react';
import {COLORS} from '../../../Assets/theme/color';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import { responsiveHeight,responsiveWidth } from 'react-native-responsive-dimensions';
import AppIntroSlider from 'react-native-app-intro-slider';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Components from '../../../Components/index'
import {useTranslation} from 'react-i18next';
import {styles} from './Styles/Sliders';

// Pour choix langue
import {RadioButton} from 'react-native-paper';
import {useDispatch, useSelector} from 'react-redux';
import allLanguages from '../../../Assets/allLanguages.json';
import {getAllLieux, updatelangue, pagecounter,} from '../../../actions';


const {width, height} = Dimensions.get('screen');

export default function Sliders (props) {

  const {t, i18n} = useTranslation();
  const dispatch = useDispatch();
  const [currentLang, setcurrentLang] = useState(null)
  const lieuxe = useSelector(state => state.lieux);
  const page = 0;


  const slides = [
    {
      key: 's1',
      image: require('../../../Assets/Images/Slider/ImageDiapo1.png'),
      title: 'Language',
      text: t('ParametresPage.langues'),
      styles: styles.introImageStyle1,
      backgroundColor: COLORS.background,
    },
    {
      key: 's2',
      image: require('../../../Assets/Images/Slider/ImageDiapo1.png'),
      title: t('Slider.titleslider1'),
      text: t('Slider.slider1'),
      styles: styles.introImageStyle1,
      backgroundColor: COLORS.background,
    },
    {
      key: 's3',
      image: require('../../../Assets/Images/Slider/ImageDiapo2.png'),
      title: t('Slider.titleslider2'),
      text: t('Slider.slider2'),
      styles: styles.introImageStyle2,
      backgroundColor: COLORS.background,
    },
  ];

  const onDone = () => {
    AsyncStorage.setItem('first_time', 'no')
    props.navigation.navigate('Thematic');
  };

  const RenderNextButton = () => {
    return (
      <View style={styles.box}>
        <Text style={styles.introTextSuivantTermine}>{t('Slider.next')}</Text>
      </View>
    );
  };

  const RenderDoneButton = () => {
    return (
      <View style={styles.box}>
        <Text style={styles.introTextSuivantTermine}>{t('Slider.done')}</Text>
      </View>
    );
  };

  const RenderSkipButton = () => {
    return (
      <View style={styles.box}>
        <Text style={styles.introTextPasser}>{t('Slider.skip')}</Text>
      </View>
    );
  };

  const langChange = (lang) => {
    setcurrentLang(lang)
    AsyncStorage.setItem('language', lang);
    i18n.changeLanguage(lang);
    dispatch(updatelangue(lang));
    dispatch(pagecounter(page));
    dispatch(
      getAllLieux(
        lieuxe.radius,
        lieuxe.thematic,
        page, // Changement ici ?
        lieuxe.longitude,
        lieuxe.latitude,
        lang, // Changement ici
        lieuxe.celebrit,
      ),
    );
    
  }

  const renderLanguageList = () => {
    currentLang == null && AsyncStorage.getItem('language').then(value => setcurrentLang(value))

    let jsxArray = []
    let i = 0
    for (const langCode in allLanguages){
      jsxArray.push(
        <View style={styles.langItem} key={i++}>
          <Text style={styles.textModalitem}>{allLanguages[langCode] /* Vrai nom */}</Text>
          <View style={{backgroundColor: currentLang == langCode ? '#45C6BE' : '#C4C4C4', borderRadius: 20}}>
            <RadioButton
              value={langCode}
              color={currentLang == langCode ? 'white' : null}
              status={currentLang == langCode ? 'checked' : 'unchecked'}
              onPress={() => langChange(langCode) }
            />
          </View>
        </View>
      )
    }
    return jsxArray
  }


  const renderItem = ({item}) => {
    return (
      <View
        style={{
          flex: 1,
          display: 'flex',
          backgroundColor: item.backgroundColor,
          alignItems: 'center',
          justifyContent: 'center',
          paddingTop: height / 10,
          paddingBottom: height / 10,
          position: 'relative',
        }}>
        
          <Components.TopBar titre="Slides"/>

          {
            // Si écran de sélection des langues
            (item.key == 's1') ?
                <View style={styles.slideBox}>
                  <Text style={styles.title}>{item.text}</Text>
                  <View style={styles.langScroll}>
                    <ScrollView style={styles.flexcolumn}>
                      { renderLanguageList() }
                    </ScrollView>
                  </View>
                </View>

            // Si display normal des slides
            : 
              <View style={styles.slideBox}>

                {/* <Text style={styles.introTitleStyle}>{item.Grostitre}</Text> */}
                {/* <Text style={styles.introTitleStyle}>{item.title}</Text>  */}
    
                <Image style={item.styles} source={item.image}/>
    
                <Text style={styles.introTextStyle2}>
                  <Text style={styles.introTextStyle2}>{item.title}</Text>
                  {item.text}
                </Text>
              </View>
            
          }
            </View>
    );
  };
  
  return (
    <AppIntroSlider
      data={slides}
      renderItem={renderItem}
      onDone={onDone}
      onSkip={onDone}
      showSkipButton={true}
      dotStyle={{backgroundColor: COLORS.darkgrey}}
      activeDotStyle={{backgroundColor: COLORS.green}}
      renderNextButton={RenderNextButton}
      renderDoneButton={RenderDoneButton}
      renderSkipButton={RenderSkipButton}
    />
  );
};

