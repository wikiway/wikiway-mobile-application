import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';
import { responsiveHeight,responsiveWidth } from 'react-native-responsive-dimensions';
const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.background,
        alignItems: 'center',
        padding: 10,
        justifyContent: 'center',
      },
      
      // titleStyle: {
      //   padding: 10,
      //   textAlign: 'center',
      //   fontSize: 18,
      //   fontWeight: 'bold',
      // },
    
      // introTitleStyle: {
      //   fontSize: 60,
      //   color: 'black',
      //   textAlign: 'center',
      //   marginBottom: 16,
      //   fontWeight: 'bold',
      // },
    
      introImageStyle1: {
        width: responsiveHeight(30),
        height: responsiveHeight(25),
      },

      introImageStyle2: {
        width: responsiveHeight(17),
        height: responsiveHeight(20.5),
      },
    
      introTextStyle: {
        fontSize: responsiveHeight(49),
        color: COLORS.green,
        textAlign: 'left',
        paddingVertical: 30,
        fontFamily: 'Poppins-Regular'
      },
    
      introTextStyle2: {
        fontSize: responsiveHeight(2.1),
        color: COLORS.black,
        textAlign: 'left',
        paddingVertical: 30,
        fontFamily: 'Poppins-Regular'
      },
    
      introTextSuivantTermine: {
        fontSize: responsiveHeight(2.5),
        color: COLORS.green,
        textAlign: 'center',
        fontFamily: 'Poppins-Regular'
      },
      
      introTextPasser: {
        fontSize: responsiveHeight(2.5),
        color: COLORS.black,
        textAlign: 'center',
        fontFamily: 'Poppins-Regular'
      },
    
      box: {
        marginTop: height / 55,
      },
    
      slideBox: {
        width: responsiveWidth(90),
        height: responsiveHeight(70),
        backgroundColor: COLORS.white,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        padding: 5,
      },


    // Pour la sélction des langues

      title: {
        marginBottom: -75,
        textAlign: 'center',
        fontSize: 22.5,
        fontWeight: '700',
        paddingVertical: responsiveHeight(0.7),
     },

      langScroll: {
        marginTop: responsiveHeight(13),
      },

      flexcolumn: {
        display: 'flex',
        flexDirection: 'column',
        padding: responsiveHeight(1),
      },

      langItem: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 7
      },

      textModalitem: {
        fontSize: width / 19,
        fontFamily:'Poppins-Regular'
      },
});