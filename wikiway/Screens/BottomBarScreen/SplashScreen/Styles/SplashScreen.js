import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../../../Assets/theme/color';



export const styles = StyleSheet.create({

  container: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.background,
    position:'relative',
    width:window.width,
    height:window.height
  },

  splash: {
    width:Platform.OS === 'ios' ?"90%": "80%", //400
    height:"100%"
  },
});
