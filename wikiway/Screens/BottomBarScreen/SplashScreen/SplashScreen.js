import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Dimensions, useWindowDimensions, Image, Asy,Alert, Text} from 'react-native';
import {styles} from './Styles/SplashScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from 'react-redux';
import { updatelangue } from '../../../actions';

const NextScreen = props => {
  
  const [loading, setLoading] = useState(true);
  const [isFirstTime, setIsFirstTime] = useState(false);
  const dispatch = useDispatch()

  const checkForFirstTImeLoad = async () => {
    const result = await AsyncStorage.getItem('first_time');
    if (result === null) setIsFirstTime(true);
    setLoading(false);
  }
  
  AsyncStorage.getItem('language').then(value => {
    if (value !== null) {
      dispatch(updatelangue(value))
    } else {
      console.log('pasdevalue');
    }
  });
  
  useEffect(() => {
    checkForFirstTImeLoad()
  })

  setTimeout(() => {
    if (loading) return null;
    if (!isFirstTime) {
      props.navigation.navigate('Thematic');
      //props.navigation.navigate('Sliders');
    } if (isFirstTime){
      props.navigation.navigate('Sliders');
    };
    },3000);
  }

const SplashScreen = props => {

  return (
    <View style={styles.container}>
        {<Image style={styles.splash} source={require('../../../Assets/Images/LogoSplashScreen/LogoWikiway5.jpg')} />}
      {NextScreen(props)}
    </View>
  );
};

export default SplashScreen;