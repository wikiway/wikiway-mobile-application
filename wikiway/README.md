# Wikiway front-end english

## Table of Contents

1. [General presentation of the application] (#overview-of-the-application)
2. [Wikiway Structure] (#structure-of-wikiway)
3. [Technologies used and Evolution of wikiway] (#technologies-used-and-evolution-of-wikiway)
4. [Functions] (#functions)
5. [Setup] (#setup)
6. [Useful links](#useful-links)
7. [Authors] (#authors)

### Overview of the application

Wikiway is an ios and android application allowing you to search for all the landmarks referenced on Wikidata located around you such as the Eiffel Tower, the Arc de Triomphe, and many more. What is available around me? You will have everything in the Wikiway app! Wikiway was created by Bivilink in partnership with Wikipedia.

### Structure of Wikiway

In this part, we show how Wikiway is structured by showing all the folders and files: how the folders are distributed and what they contain.

| Folder          | Descriptions                                                            |
|-----------------|-------------------------------------------------------------------------|
| android         | Contains files for generates a android application                      |
| ios             | Contains files for generates a ios application                          |
| assets          | Contains all images, text or color in the app                           |
| action          | Contains all requests for call the api back-end                         |
| screens         | Contains all views                                                      |
| node_modules    | A folder who allow to generate all the dependencies                     |
| components      | Contains functions used in views                                        |
| reducers        | Contains redux and is usefull for update value in store thanks to redux |
| navigation      | Contains navigations app                                                |

    * files and their contents

    **App.js** : allow to init pages in the app.

    **package.json** : list all installed dependencies.

    **Home.js** : View who list all monuments in the app. 

    **RayonModal.js** : View and setup for radius.

    **Form.js** : View who return a form for errors user.  

    **DetailLieu.js** : View for each monument.

    **Langues.js** : List and allows to use all languages in the app.


### Technologies used and Evolution of Wikiway

| Versions             | Technologies used and specificities                                                                      |
|----------------------|----------------------------------------------------------------------------------------------------------|
| Version 1 of wikiway | Version 1 of wikiway Version with the listed landmarks sortable using filters and categories (“themes”). |
| Release 2            | in progress                                                                                              |


### Functions

| Important functions to know           | roles                                                                        |
|---------------------------------------|------------------------------------------------------------------------------|
| *getAllLieux() in lieux.action.js*    | Allows to list all the monuments                                             |
| *getLieuxbyid() in lieux.action.js*   | Allow to find monuments by url                                               |
| *updatetitle() in lieux.action.js*    | Update of the value title Monument                                           |
| *updateradius() in lieux.action.js*   | Update of the value radius                                                   |
| *updatethematic() in lieux.action.js* | Update value thematics                                                       |
| *updatelonglat() in lieux.action.js*  | Update gps coordinates user  (longitude and lattitude)                       |
| *articleid() in lieux.action.js*      | Get the url article                                                          |
| *updatelangue() in lieux.action.js*   | Update languages                                                             |
| *pagecounter() in lieux.action.js*    | function that allows to create few pages in the app for manage getAllLieux() |

### Setup

1. Install [Node.js] (https://nodejs.org/en/download/).
2. Create a gitlab account.
3. Install [Git](https://git-scm.com/download/win) if needed.
4. Type the command "git clone https://gitlab.com/wikiway/wikiway-mobile-application.git" in any folder.
5. Identify yourself (Git username and password) to create a wikiway-mobile-application folder for you.
6. Open your IDE and then select "Open..." search for the Wikiway-mobile-application project installed on your pc beforehand.
7. Then you will need to install all the dependecies for that you need to type the command "npm install" in the folder wikiway and normally a folders named "nodes_modules" appears.
8. Your projet is setup !

*For lauch the app ios

9. You need a mac and the software Xcode.
10. For open the project in xcode : select open in xcode and find and open the file named "wikiway.xcworkspace" (in IOS folder).
11. go to you cmd and enter in the wikiway-mobile-application folder then run this "cd Wikiway" and then " npx react-native run-ios".  
12. your app is lauch on ios !

*For build the ipa

13. For build the ipa, go to xcode and select the onglet "Product" on the top bar then "Destination" and for finish select "Any IOS Device (arm64)".
14. Then go again in the onglet "Product" and now select "Archive".
15. Wait the build.
16. For see your archive, go to the onglet named "Window" and then "organizer"
17. finally, Select your archive validate her and then distrubute her.

*For lauch the app android

9. you need to install [Android studio] (https://developer.android.com/studio).
10. Configure the ANDROID_HOME environment variable you can find that on internet (https://archive.reactnative.dev/docs/getting-started).
11. Install a "device manager" in your android studio i recommand you the "pixel 6 pro" with the latest API version.   
12. go to you cmd and enter in the wikiway-mobile-application folder then run this "cd Wikiway" and then " npx react-native run-android".
13. you can encounter few bug for exemple the simulator don't detect metro (react native start) or the metro don't run alone.
14. if the simulator don't detect the npm start go to the simulator and change the ip of simulator with 10.0.2.2:8081 for that enter ctrl + m in the simulator and then select debug server host & port device.
15. and make sure you have metro (react native start) run for that run the command "npm start" in the folder wikiway.
16. your app is lauch on android !

*For build the apk 

17. Before created the build, you need to generate a keystore for that follow the link https://reactnative.dev/docs/signed-apk-android.
18. Change the command with "keytool -genkeypair -v -storetype PKCS12 -keystore wikiway.keystore -alias wikiway-alias -keyalg RSA -keysize 2048 -validity 10000"
if they ask a password type "Wikiway12345".
19. copy the wikiway.keystore add it on path "/android/app/". 
20. now go to the android folder ("cd Android" in wikiway folder).
21. Then type the command "./gradlew bundleRelease".
22. When the build is done. Go to "/android/app/build/bundle/app-release.aab".
23. Upload on google play console website the file named app-release.aab for create a new app release.

### Useful links

Setup react-native : https://archive.reactnative.dev/docs/getting-started
For problems with networking with simulator android : https://developer.android.com/studio/run/emulator-networking
For Redux : https://redux.js.org/introduction/getting-started
Publish on google play store : https://reactnative.dev/docs/signed-apk-android
Publish on App store :https://reactnative.dev/docs/publishing-to-app-store

usefully command : adb reverse tcp:5000 tcp:5000

### Authors

RAJANANTHAN KEVIN (developer front-end) 
MAFFEI Enzo (developer full-stack)

## Wikiway front-end français

## Table des matières

1. [Présentation générale de l'application] (#présentation-générale-de-l'application)
2. [Structure de Wikiway] (#structure-de-wikiway)
3. [Technologies utilisées et évolution de wikiway] (#technologies-utilisées-et-evolution-de-wikiway)
4. [Fonctions] (#fonctions)
5. [Installation] (#configuration)
6. [Liens utiles](#liens-utiles)
7. [Auteurs] (#auteurs)

### Présentation générale de l'application

Wikiway est une application ios et android vous permettant de rechercher tous les points de repère référencés sur Wikidata situés autour de vous tels que la Tour Eiffel, l'Arc de Triomphe, et bien d'autres. Qu'est-ce qui est disponible autour de moi ? Vous aurez tout dans l'application Wikiway ! Wikiway a été créé par Bivilink en partenariat avec Wikipédia.

### Structure de Wikiway

Dans cette partie, nous montrons comment Wikiway est structuré en montrant tous les dossiers et fichiers : comment les dossiers sont distribués et ce qu'ils contiennent.

| Dossier         | Descriptions                                                                   |
|-----------------|--------------------------------------------------------------------------------|
| android         | Contient les fichiers pour générer une application android                     |
| ios             | Contient les fichiers pour générer une application ios                         |
| assets          | Contient toutes les images, textes ou couleurs de l'application                | 
| action          | Contient toutes les requêtes pour appeler l'api back-end                       |
| screens         | contient toutes les vues                                                       |
| node_modules    | Un dossier qui génère toutes les dépendances                                   |
| components      | Contient les fonctions utilisées dans les vues                                 |
| reducers        | Contient redux et est utile pour mettre à jour la valeur stocker grâce à redux |
| navigation      | contient les fichiers de navigation de l'application et leur contenu           |


    * Fichiers et leur contenu

    **App.js** : permet d'initialiser les pages de l'application.

    **package.json** : liste toutes les dépendances installées.

    **Home.js** : Vue qui liste tous les monuments dans l'app. 

    **RayonModal.js** : Visualisation et configuration du radius.

    **Form.js** : Vue qui retourne un formulaire pour les erreurs de l'utilisateur.  

    **DetailLieu.js** : Vue pour chaque monument.

    **Langues.js** : Liste et permet d'utiliser toutes les langues dans l'application.


### Technologies utilisées et Evolution de Wikiway

| Versions             | Technologies utilisées et spécificités                                                                         |
|----------------------|----------------------------------------------------------------------------------------------------------------|
| Version 1 de wikiway | Version 1 de wikiway Version avec les repères listés triables à l'aide de filtres et de catégories ("thèmes"). |
| Version 2            | en cours                                                                                                       |                          


### Fonctions

| Fonctions importantes à connaître       | rôles                                                                                      |
|-----------------------------------------|--------------------------------------------------------------------------------------------|
| *getAllLieux() in lieux.action.js*      | Permet de lister tous les monuments                                                        |
| *getLieuxbyid() in lieux.action.js*     | Permet de trouver des monuments par url                                                    |
| *updatetitle() in lieux.action.js*      | Mise à jour de la valeur title Monument                                                    |
| *updateradius() in lieux.action.js*     | Mise à jour de la valeur rayon                                                             |
| *updatethematic() in lieux.action.js*   | Mise à jour de la valeur thematics                                                         |
| *updatelonglat() in lieux.action.js*    | Mise à jour des coordonnées gps utilisateur (longitude et lattitude)                       |
| *articleid() in lieux.action.js*        | Obtenir l'url de l'article                                                                 |
| *updatelangue() in lieux.action.js*     | Mise à jour des langues                                                                    |
| *pagecounter() in lieux.action.js*      | Fonction qui permet de créer quelques pages dans l'application pour gérer getAllLieux()    |


### Configuration

1. Installer [Node.js] (https://nodejs.org/en/download/).
2. Créer un compte gitlab.
3. Installez [Git] (https://git-scm.com/download/win) si nécessaire.
4. Tapez la commande "git clone https://gitlab.com/wikiway/wikiway-mobile-application.git" dans n'importe quel dossier.
5. Identifiez-vous (nom d'utilisateur et mot de passe Git) pour créer un dossier wikiway-mobile-application pour vous.
6. Ouvrez votre IDE puis sélectionnez "Ouvrir..." recherchez le projet Wikiway-mobile-application préalablement installé sur votre pc.
7. Ensuite vous devrez installer toutes les dépendances pour cela vous devez taper la commande "npm install" dans le dossier wikiway et normalement un dossier nommé "nodes_modules" apparaît.
8. Votre projet est prêt !

*Pour lancer l'application ios

9. Vous avez besoin d'un mac et du logiciel Xcode.
10. Pour ouvrir le projet dans xcode : sélectionnez open dans xcode après cela trouvez et ouvrez le fichier nommé "wikiway.xcworkspace" (Dans le dossier ios).
11. allez dans votre cmd et entrez dans le dossier wikiway-mobile-application puis exécutez ceci : "cd Wikiway" et ensuite " npx react-native run-ios".  
12. votre application est lancée sur ios !

*Pour construire l'ipa

13. Pour construire l'ipa, allez sur xcode et sélectionnez l'onglet "Product" sur la barre supérieure puis "Destination" et pour finir sélectionnez "Any IOS Device (arm64)".
14. Puis allez de nouveau dans l'onglet "Product" et sélectionnez maintenant "Archive".
15. Attendez le build.
16. Pour voir votre archive, allez dans l'onglet nommé "Windows" et ensuite "organizer". 17.
17. enfin, sélectionnez votre archive validez-la et ensuite distribuez-la.

*Pour lancer l'application sur Android

9. vous devez installer [Android studio] (https://developer.android.com/studio).
10. Configurer la variable d'environnement ANDROID_HOME que vous pouvez trouver sur internet (https://archive.reactnative.dev/docs/getting-started).
11. Installez un "device manager" dans votre studio android je vous recommande le "pixel 6 pro" avec la dernière version de l'API.   
12. allez dans votre cmd et entrez dans le dossier wikiway-mobile-application puis exécutez ceci "cd Wikiway" et ensuite " npx react-native run-android".
13. vous pouvez rencontrer quelques bugs par exemple le simulateur ne détecte pas metro (react native start) ou le metro ne fonctionne pas tout seul.
14. si le simulateur ne détecte pas le démarrage de npm, allez dans le simulateur et changez l'ip du simulateur avec 10.0.2.2:8081 pour cela entrez ctrl + m dans le simulateur et ensuite sélectionnez debug server host & port device.
15. et assurez-vous que vous avez metro (react native start) démarrer pour cela exécutez la commande "npm start" dans le dossier wikiway.
16. votre application est lancée sur android !

*Pour construire l'apk 

17. Avant de créer le build, vous devez générer un keystore pour cela suivez le lien https://reactnative.dev/docs/signed-apk-android.
18. Changez la commande avec "keytool -genkeypair -v -storetype PKCS12 -keystore wikiway.keystore -alias wikiway-alias -keyalg RSA -keysize 2048 -validity 10000"
s'ils demandent un mot de passe, tapez "Wikiway12345".
19. copiez la wikiway.keystore et ajoutez-la au chemin "/android/app/". 
20. allez maintenant dans le dossier android ("cd Android" dans le dossier wikiway).
21. Puis tapez la commande "./gradlew bundleRelease".
22. Quand la construction est terminée. Allez dans "/android/app/build/bundle/app-release.aab".
23. Téléchargez sur le site Google Play console le fichier nommé app-release.aab pour créer une nouvelle version de l'app.

### Liens utiles

Configuration de react-native : https://archive.reactnative.dev/docs/getting-started
Pour les problèmes de réseau avec le simulateur android : https://developer.android.com/studio/run/emulator-networking
Pour Redux : https://redux.js.org/introduction/getting-started
Publier sur le google play store : https://reactnative.dev/docs/signed-apk-android
Publier sur l'App store :https://reactnative.dev/docs/publishing-to-app-store

commande utile : adb reverse tcp:5000 tcp:5000

### Auteurs

RAJANANTHAN KEVIN (développeur front-end) 
MAFFEI Enzo (développeur full-stack)