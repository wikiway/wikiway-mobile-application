import React from 'react';
import * as Screens from '../Screens/AllScreens';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

const HomeStackNavigator = () => {
    return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Home" component={Screens.Home} options={{gestureEnabled: false}}/>
    </Stack.Navigator>
    
  );
  };

  const VoiceStackNavigator = () => {
    return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Voice" component={Screens.Voice} options={{gestureEnabled: false}}/>
    </Stack.Navigator>
    
  );
  };

  const RetroStackNavigator = () => {
    return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Retro" component={Screens.Retro} options={{gestureEnabled: false}}/>
    </Stack.Navigator>
    
  );
  };

  const ParametresStackNavigator = () => {
    return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Parametres" component={Screens.Parametres} options={{gestureEnabled: false}}/>
    </Stack.Navigator>
    
  );
  }

  export {HomeStackNavigator, VoiceStackNavigator, RetroStackNavigator, ParametresStackNavigator};

