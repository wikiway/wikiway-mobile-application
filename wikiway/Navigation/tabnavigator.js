import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Dimensions, View} from 'react-native';
import HomeActive from '../Assets/Images/BottomBar/HomeActive.svg';
import HomeInactive from '../Assets/Images/BottomBar/HomeInactive.svg';
import VoiceActive from '../Assets/Images/BottomBar/VoiceActive.svg';
import VoiceInactive from '../Assets/Images/BottomBar/VoiceInactive.svg';
import RetroActive from '../Assets/Images/BottomBar/RetroActive.svg';
import RetroInactive from '../Assets/Images/BottomBar/RetroInactive.svg';
import ParametresActive from '../Assets/Images/BottomBar/ParametresActive.svg';
import ParametresInactive from '../Assets/Images/BottomBar/ParametresInactive.svg';
import {
  HomeStackNavigator,
  VoiceStackNavigator,
  RetroStackNavigator,
  ParametresStackNavigator,
} from './navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Text} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import {useTranslation} from 'react-i18next';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';

const Tab = createBottomTabNavigator();

const {width, height} = Dimensions.get('screen');

const Nav = (props, {navigation}) => {
  const [language, setLanguage] = React.useState('');
  const {t, i18n} = useTranslation();

  return (
    <Tab.Navigator
      tabBarOptions={{
        style: 
          {height: responsiveHeight(10)}
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => {
          let svg1;
          

          if (route.name === 'Home') {
            svg1 = focused ? (
              <HomeActive width={responsiveHeight(11)} height= {responsiveHeight(5)} />
            ) : (
              <HomeInactive width={responsiveHeight(11)} height= {responsiveHeight(5)}  />
            );
          } else if (route.name === 'Voice') {
            svg1 = focused ? (
              <VoiceActive width={width / 11} />
            ) : (
              <VoiceInactive width={width / 11} />
            );
          } else if (route.name === 'Retro') {
            svg1 = focused ? (
              <RetroActive width={width / 11}  />
            ) : (
              <RetroInactive width={width / 11}  />
            );
          } else if (route.name === 'Parametres') {
            svg1 = focused ? (
              <ParametresActive width={responsiveHeight(11)} height= {responsiveHeight(5)} />
            ) : (
              <ParametresInactive width={responsiveHeight(11)} height= {responsiveHeight(5)}/>
            );
          }
          return (
            <View
              style={{
                backgroundColor:'white',
                height: responsiveHeight(10),
                width: responsiveWidth(70) ,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: responsiveHeight(3),
                paddingRight: height/100,
                bottom: 0,
                }}>
              


              {svg1}

             
              {route.name === 'Home' ? (
                <Text style={{color: focused ? '#45C6BE' : '#BCBDC7',fontWeight:'bold',paddingTop: 2,textAlign:'center'}}>{t('BottomBar.accueil')}</Text>
              ) : null}
             
              {route.name === 'Voice' ? (
                <Text style={{color: focused ? '#45C6BE' : '#BCBDC7',fontWeight:'bold',paddingTop: 2,textAlign:'center'}}>{t('BottomBar.wikiaudio')}</Text>
              ) : null}

              
              {route.name === 'Retro' ? (
                <Text style={{color: focused ? '#45C6BE' : '#BCBDC7',fontWeight:'bold',paddingTop: 2,textAlign:'center'}}>{t('BottomBar.retro')}</Text>
              ) : null}

              {route.name === 'Parametres' ? (
                <Text style={{color: focused ? '#45C6BE' : '#BCBDC7',fontWeight:'bold',paddingTop: 2,textAlign:'center'}}>{t('BottomBar.parametre')}</Text>
              ) : null}
             
            </View>
          );
        },
      })}>
        
      <Tab.Screen
        options={{
          tabBarLabel: '',
        }}
        name="Home"
        component={HomeStackNavigator}
      />
      <Tab.Screen
        options={{
          tabBarLabel: '',
        }}
        name="Parametres"
        component={ParametresStackNavigator}
      />
    </Tab.Navigator>
  );
};
/*
<Tab.Screen
        options={{
          tabBarLabel: '',
        }}
        name="Retro"
        component={RetroStackNavigator}
      />
      <Tab.Screen
        options={{
          tabBarLabel: '',
        }}
        name="Voice"
        component={VoiceStackNavigator}
      />
      */

export default Nav;
