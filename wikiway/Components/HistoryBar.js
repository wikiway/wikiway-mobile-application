import React, {useState} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  Platform,
  Image,
  TouchableOpacity,
} from 'react-native';

const {width, height} = Dimensions.get('screen');

import ActiveListIcon from '../Assets/Images/Retro/listActive.svg';
import InactiveListIcon from '../Assets/Images/Retro/listInactive.svg';
import InactiveGridIcon from '../Assets/Images/Retro/gridInactive.svg';
import ActiveGridIcon from '../Assets/Images/Retro/gridActive.svg';
import {useTranslation} from 'react-i18next';
import {styles} from './Styles/HistoryBar.js';

const HistoryBar = props => {
    const {t} = useTranslation();

 return (

    <View style={styles.minicontainerPosition1}>
      <Text style={styles.text} >{props.text}</Text>
      <View style={styles.miniminicontainerPosition2}>
        <TouchableOpacity   onPress={props.onPress}>
        { props.value ?
          <ActiveListIcon width={width / 11} height={height / 23} /> :
          <InactiveListIcon width={width / 11} height={height / 23} /> 
        }
        </TouchableOpacity>
        <TouchableOpacity onPress={props.onPress2}>
        { props.value2 ?
          <ActiveGridIcon width={width / 13} height={height / 23} /> :
          <InactiveGridIcon width={width / 13} height={height / 23} /> 
        }
        </TouchableOpacity>
      </View>
    </View>

     /* <View style={styles.minicontainerPosition1}>
            <Text>{props.text}</Text>
            <View style={styles.miniminicontainerPosition2}>
              <TouchableOpacity onPress={() => listFunction()}>
                {list ? (
                  <ActiveListIcon width={width / 11} height={height / 23} />
                ) : (
                  <InactiveListIcon width={width / 11} height={height / 23} />
                )}
              </TouchableOpacity>
              <TouchableOpacity onPress={() => gridFunction()}>
                {grid ? (
                  <ActiveGridIcon width={width / 13} height={height / 23} />
                ) : (
                  <InactiveGridIcon width={width / 13} height={height / 23} />
                )}
              </TouchableOpacity>
            </View>
          </View> */
  );
};

export default HistoryBar;
