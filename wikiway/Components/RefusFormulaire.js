import React from 'react';
import {
  View,
  Text,
  Modal,
  Dimensions,
  StyleSheet
} from 'react-native';
import Wrong from '../Assets/Images/Form/false.svg';
const {width, height} = Dimensions.get('screen');
import {styles} from './Styles/RefusFormulaire';



const RefusFormulaire = (props) => {
  return (
    <View style={styles.container}>
      <Modal transparent={true} visible={props.value ? true : false}>
        <View style={{backgroundColor: 'white', flex: 1}}>
          <View style={styles.block}>
            <View style={styles.logo}>
              <Wrong width={width} height={"100%"}
              />
            </View>
            <Text style={styles.text}>{props.text}</Text>
          </View>
        </View>
      </Modal>
    </View>
  );
};





export default RefusFormulaire;