import React from 'react';
import {
  View,
  Text,
  Modal,
  Dimensions,
  StyleSheet
} from 'react-native';
import True from '../Assets/Images/Form/true.svg';

const {width, height} = Dimensions.get('screen');

const ValidationFormulaire = (props) => {
  return (
    <View style={styles.container}>
      <Modal transparent={true} visible={props.value ? true : false}>
        <View style={{backgroundColor: 'white', flex: 1}}>
          <View style={styles.block}>
            <View style={styles.logo}>
              <True width={width} height={"100%"}
              />
            </View>
            <Text style={styles.text}>{props.text}</Text>
          </View>
        </View>
      </Modal>
    </View>
  );
};


export const styles = StyleSheet.create({
    block: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    logo: {
        height: height/12,
        width: width,
        marginBottom: height/35,
    },
    text: {
        fontSize: height/50,
        fontFamily: 'Poppins-Medium',
        color:'#978D8D'
    }
})


export default ValidationFormulaire;