import React, {useState} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  Platform,
  Image,
  TouchableOpacity,
  TextPropTypes,
} from 'react-native';
import {styles} from './Styles/BubbleThematic';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';


const {width, height} = Dimensions.get('screen');

const bullethematique = props => {
  return (
    <>
    <TouchableOpacity
    onPress={props.onPress}>
    <Image style={{width: responsiveWidth(43),
    height: responsiveHeight(13.3)}} source={props.image}/>
      <View
        style={{
          width: responsiveWidth(43),
          height: responsiveHeight(6),
          borderBottomEndRadius: height/25,
          borderBottomLeftRadius: height/25,
          backgroundColor: props.value ? '#45C6BE' : '#ffffff',
          marginBottom: height / 100,
          justifyContent: 'space-around',
        }}>
        
        <View style={styles.test}>
        <Text style={styles.text}>{props.titre}</Text>
        </View>
      </View>
    </TouchableOpacity>
    </>
  );
};
export default bullethematique;
