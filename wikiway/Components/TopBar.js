import React, {useState} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  Platform,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import Select from '../Assets/Images/TopBar/Select.svg';
import TopBarLogo from '../Assets/Images/TopBar/logosansecriture.svg';
import RayonIcon from '../Assets/Images/TopBar/lunette3.png';
import FormIcon from '../Assets/Images/TopBar/form.svg';
import ShareIcon from '../Assets/Images/TopBar/share.svg';
import VisioIcon from '../Assets/Images/TopBar/logosansecriture.svg';
import BackIcon from '../Assets/Images/TopBar/back.svg';
import ModalRayon from '../Screens/BottomBarScreen/Home/RayonModal';
import InformationModal from '../Screens/BottomBarScreen/WikiAudio/InformationModal';
import InformationModal2 from '../Screens/BottomBarScreen/Retro/InformationModal2';
import AudioIcon from '../Assets/Images/TopBar/audio.svg';
import EditIcon from '../Assets/Images/TopBar/pentosquareregular.svg'
import ThematicIcon from '../Assets/Images/TopBar/fenetre.png';
import InfoIcon from '../Assets/Images/TopBar/Info.svg';
import RetroLogo from '../Assets/Images/TopBar/retroLogos.svg';
import {useTranslation} from 'react-i18next';
import {styles} from './Styles/TopBar';

const {width, height} = Dimensions.get('screen');

const topbar = props => {
  const {t} = useTranslation();
  const [aff, setAff] = useState(0);
  const modal = () => {
    setAff(aff + 1);
    console.log(aff);
  };

  if (props.titre == 'Thématiques') {
    return (
      <View style={styles.container}>
        <Text style={styles.acc}>{t('thematiquePage.topbar.thematique')}</Text>
      </View>
    );
  } else if (props.titre == 'Slides') {
    return (
      <View style={styles.container}>
        <View style={styles.containerLogo}> 
          <Image style={styles.containerimageTopbarlogo} source={require('../Assets/Images/TopBar/TopBarLogo.png')}/>
        </View>
      </View>
    );
  } else if (props.titre == 'Slides2') {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={responsiveWidth(8)} height={height / 35} />
        </TouchableOpacity>
        <View style={styles.containerLogo}> 
          <Image style={styles.containerimageTopbarlogo} source={require('../Assets/Images/TopBar/TopBarLogo.png')}/>
        </View>
      </View>
    );
  } else if (props.titre == 'Accueil') {
    return (
      <View style={styles.container2}>
        <TouchableOpacity style={styles.bouton2}  onPress={props.onPress2}>
            <Image style={styles.imagethematic} source={require('../Assets/Images/TopBar/fenetre3.png')}/>
        </TouchableOpacity>
        <Image style={styles.acc2} source={require('../Assets/Images/TopBar/TopBarLogo.png')}/>
        <TouchableOpacity style={styles.bouton} onPress={() => modal()}>
          <Image style={styles.imagemodal} source={require('../Assets/Images/TopBar/bonhomewiki.png')}/>
        </TouchableOpacity>
        <View>
          <ModalRayon value={aff}/>
        </View>
      </View>
    );
  } else if (props.titre == 'Accueil2') {
    return (
      <View style={styles.container2}>
        <View style={styles.bouton2}>
            <Image style={styles.imagethematic} source={require('../Assets/Images/TopBar/fenetre3.png')}/>
        </View>
        <Image style={styles.acc2} source={require('../Assets/Images/TopBar/TopBarLogo.png')}/>
        <View style={styles.bouton}>
          <Image style={styles.imagemodal} source={require('../Assets/Images/TopBar/bonhomewiki.png')}/>
        </View>
        <View>
          <ModalRayon value={aff}/>
        </View>
      </View>
    );
  } else if (props.titre == 'WebView') {
    return(
      <View style={styles.container2}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={responsiveHeight(4)} height={responsiveHeight(3.5)} />
        </TouchableOpacity>
      </View>
    );
  } else if (props.titre == 'detailLieu') {
    return (
      <View style={styles.container3}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={responsiveHeight(4)} height={responsiveHeight(3.5)} />
        </TouchableOpacity>
        <Image style={styles.acc3} source={require('../Assets/Images/TopBar/TopBarLogo.png')}/>
        {/* <Text>LOL</Text> */}
        <View style={styles.displays}>
          <TouchableOpacity style={styles.bouton1} onPress={props.onPress0}>
            <EditIcon width={responsiveHeight(4)} height={responsiveHeight(4)} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.bouton6} onPress={props.onPress1}>
            <FormIcon width={responsiveHeight(4)} height={responsiveHeight(4)} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.bouton3} onPress={props.onPress2}>
            <ShareIcon width={responsiveHeight(4)} height={responsiveHeight(4)} />
          </TouchableOpacity>
        </View>
      </View>
    );
  } else if (props.titre == 'Form') {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={width / 22} height={height / 35} />
        </TouchableOpacity>
        <Text style={{fontFamily:'Poppins-SemiBold'}}>
          {t('FormPage.suggestionError')}
        </Text>
      </View>
    );
  } else if (props.titre == 'wikiaudio') {
    return (
      <View style={styles.container}>
        <AudioIcon width={width / 2.5} height={height / 2.5} />
        {/* <Text>LOL</Text> */}
        <View style={styles.displays}>
          <TouchableOpacity style={styles.bouton2} onPress={props.onPress1}>
            <FormIcon width={width / 15} height={height / 25} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.bouton3} onPress={() => modal()}>
            <InfoIcon width={width / 11} height={height / 21} />
          </TouchableOpacity>
        </View>
        <View>
          <InformationModal value={aff} />
        </View>
      </View>
    );
  } else if (props.titre == 'retro') {
    return (
      <View style={styles.container}>
        <RetroLogo width={width / 2.5} height={height / 2.5} />
        {/* <Text>LOL</Text> */}
        <View style={styles.displayss}>
          <TouchableOpacity style={styles.bouton3} onPress={() => modal()}>
            <InfoIcon width={width / 11} height={height / 21} />
          </TouchableOpacity>
        </View>
        <View>
          <InformationModal2 value={aff} />
        </View>
      </View>
    );
  } else if (props.titre == 'Paramètres') {
    return (
      <View style={styles.container}>
        <Text style={styles.acc}>{t('ParametresPage.parametre')}</Text>
      </View>
    );
  } else if (props.titre == 'thematique2') {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={width / 22} height={height / 35} />
        </TouchableOpacity>
        <Text style={styles.acc}>{t('ParametresPage.thematique')}</Text>
      </View>
    );
  }
  else if (props.titre == 'Voix') {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={width / 22} height={height / 35} />
        </TouchableOpacity>
        <Text style={styles.acc}>{t('ParametresPage.voix')}</Text>
      </View>
    );
  }
  else if (props.titre == 'Luminosite') {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={width / 22} height={height / 35} />
        </TouchableOpacity>
        <Text style={styles.acc}>{t('ParametresPage.luminosite')}</Text>
      </View>
    );
  }
  else if (props.titre == 'Langue') {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={width / 22} height={height / 35} />
        </TouchableOpacity>
        <Text style={styles.acc}>{t('ParametresPage.langues')}</Text>
      </View>
    );
  }
  else if (props.titre == 'LangueIntro') {
    return (
      <View style={styles.container}>
        <Text style={styles.acc}>{t('ParametresPage.langues')}</Text>
      </View>
    );
  }
  else if (props.titre == 'PolitiqueConfidentialite') {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={width / 22} height={height / 35} />
        </TouchableOpacity>
        <Text style={styles.acc}>{t('ParametresPage.politiqueConfidentialite')}</Text>
      </View>
    );
  }
  else if (props.titre == 'ConditionUtilisation') {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={width / 22} height={height / 35} />
        </TouchableOpacity>
        <Text style={styles.acc}>{t('ParametresPage.conditionsUtilisation')}</Text>
      </View>
    );
  }
  else if (props.titre == 'Description des thematiques') {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={width / 22} height={height / 35} />
        </TouchableOpacity>
        <Text style={styles.acc}>{t('ParametresPage.descriptionthematique')}</Text>
      </View>
    );
  }
  else if (props.titre == 'MentionsLegales') {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.bouton4} onPress={props.onPress}>
          <BackIcon width={width / 22} height={height / 35} />
        </TouchableOpacity>
        <Text style={styles.acc}>{t('ParametresPage.mentionLegales')}</Text>
      </View>
    );
  }
};

export default topbar;
