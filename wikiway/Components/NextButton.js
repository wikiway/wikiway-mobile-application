import React, {useState} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  Platform,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from './Styles/NextButton';
const {width, height} = Dimensions.get('screen');

const NextButton = props => {
  return (
    <TouchableOpacity style={styles.viewparents} onPress={props.onPress}>
      <Text style={styles.text}>{props.text}</Text>
    </TouchableOpacity>
  );
};

export default NextButton;
