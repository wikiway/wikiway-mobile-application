import React from 'react'
import {Text, View, TouchableOpacity} from 'react-native';
import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('screen');
import {styles} from './Styles/Button.js';

const Button = (props) => {
  if (props.type == 'return') {
    return (
      <TouchableOpacity style={styles.button} onPress={props.onPress}>
        <Text style={styles.textreturn}>
          {props.text}
        </Text>
      </TouchableOpacity>
    );
 } else if (props.type == 'check') {
    return (
      <TouchableOpacity style={!props.value ? styles.buttonRightgrey : styles.buttonRight} onPress={props.onPress} >
        <Text style={!props.value ? styles.textgrey : styles.textblue}>
          {props.text}
        </Text>
      </TouchableOpacity>
    );
  }
}

export default Button

