import {StyleSheet, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
    
    musicpanel: {
        // backgroundColor: 'red',
        width: width,
        marginTop: height / 10,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft:width/45,
        paddingRight:width/45,
        position: 'relative'
      },
    
      leftdiv: {
        width: width/4,
        height:height/25,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 4
        // backgroundColor: 'purple',
      },
    
      rightdiv: {
        width: width/4,
        height:height/25,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop:4
        // backgroundColor: 'orange',
      },
    
      flexDirections: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      },
    
      divbutton:{
        position: 'absolute',
        left: width/2.5,
        top:-(height/25)
      },
      lieutext: {
        fontFamily: 'Poppins-Regular'
      }
    
});