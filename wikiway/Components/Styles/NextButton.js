import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../Assets/theme/color';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
    viewparents: {
        backgroundColor: COLORS.green,
        width: width,
        borderTopColor: '#F0F0F0',
        paddingTop: responsiveHeight(2),
        paddingBottom: responsiveHeight(3),
        alignItems: 'center',
      },
      text: {
        color: 'white',
        fontSize: width/20,
        fontFamily: 'Poppins-Medium'
      }
    
});