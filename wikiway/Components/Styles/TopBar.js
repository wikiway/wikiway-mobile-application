import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../Assets/theme/color';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: width,
    height: responsiveHeight(8),
    top: Platform.OS === 'ios' ? 35 : 0,
    position: 'absolute',
    backgroundColor: COLORS.white,
    // paddingTop: height / 25,
  },
  container2: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: width,
    height: responsiveHeight(9),
    top: Platform.OS === 'ios' ? height / 25 : 0,
    left: 0,
    position: 'absolute',
    backgroundColor: COLORS.white,
    // paddingTop: height / 25,
  },
  container3: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: width,
    height: responsiveHeight(9),
    top: Platform.OS === 'ios' ? height / 25 : 0,
    left: 0,
    position: 'absolute',
    backgroundColor: COLORS.white,
    // paddingTop: height / 25,
  },
  imagethematic: {
    display: 'flex',
    flexDirection: 'row',
    left: responsiveWidth(7),
    width: responsiveHeight(3.5),
    height: responsiveHeight(5.6),
  },
  containerimageTopbarlogo: {
    marginTop: responsiveWidth(0),
    width: responsiveWidth(7),
    height: responsiveHeight(7),
  },
  imagemodal: {
    display: 'flex',
    flexDirection: 'row',
    left: responsiveWidth(3),
    width: responsiveHeight(4.3),
    height: responsiveHeight(6.2),
  },

  svg: {
    position: 'absolute',
    right: 0,
    bottom: -16,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },

  containerLogo: {
    width: '10%',
    height: height,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },

  thematic: {
    textAlign: 'center',
    fontSize: 10,
    fontFamily: 'Poppins-SemiBold',
  },
  acc2: {
    flexDirection: 'row',
    width: responsiveHeight(4.1),
    height: responsiveHeight(7.3),
    left: -responsiveWidth(7.7),
  },
  acc3: {
    flexDirection: 'row',
    width: responsiveWidth(6),
    height: responsiveHeight(7),
  },
  acc: {
    marginTop: Platform.OS === 'ios' ? responsiveHeight(1) : 0,
    fontSize: responsiveFontSize(2.5),
    fontFamily: 'Poppins-semibold',
  },

  bouton: {
    fontSize: height / 50,
    // marginLeft: 10
    position: 'absolute',
    right: responsiveWidth(3),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: responsiveHeight(5),
    width: responsiveHeight(8),
  },
  bouton2: {
    display: 'flex',
    position: 'relative',
    right: responsiveWidth(3),
    height: responsiveHeight(5),
    width: responsiveHeight(8),
  },
  bouton1: {
    display: 'flex',
    right: responsiveWidth(7),
  },
  bouton3: {
    display: 'flex',
  },
  bouton4: {
    position: 'absolute',
    left: responsiveWidth(3),
  },
  bouton6: {
    right: responsiveWidth(3),
  },
  toplogo: {
    width: width / 5,
    height: height / 5,
  },
  displays: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    right: 10,
  },
  displayss: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    right: 30,
  },
});
