import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
    
    block: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    logo: {
        height: height/12,
        width: width,
        marginBottom: height/35,
    },
    text: {
        fontSize: height/50,
        fontFamily: 'Poppins-Medium',
        color:COLORS.thematicTextColor
    }
    
});
