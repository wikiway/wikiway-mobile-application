import {StyleSheet, Dimensions} from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
  viewparent: {
    height: responsiveHeight(50),
    width: responsiveWidth(50),
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: height/15,
    padding: height/ 15,
  },

  text: {
    fontSize: responsiveHeight(30)/ 17,
    textAlign: 'center',
    fontFamily: 'Poppins-SemiBold',
  },

  test: {
    display: 'flex',
    justifyContent: "flex-end",
  }
});
