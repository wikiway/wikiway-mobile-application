import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
    minicontainerPosition1: {
        marginTop: height / 30,
        width: width/1.1,
        // backgroundColor: 'red',
        display: 'flex',
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: width/45,
        paddingRight: width/45,
        borderBottomWidth: 1,
        borderColor: '#C3B9B9',
        marginBottom: height / 50,
      },
      miniminicontainerPosition2:{
        width:width / 5,
        display: 'flex',
        flexDirection:'row',
        alignItems:'center',
        justifyContent: 'space-between'
    
      },

      background:{
          backgroundColor:'red'
      },

      text:{
        fontFamily:'Poppins-Bold',
        color:COLORS.textColor
      }
});