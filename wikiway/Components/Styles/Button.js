import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '../../Assets/theme/color';

const {width, height} = Dimensions.get('screen');

export const styles = StyleSheet.create({
    button: {
        borderTopWidth: 1,
        borderRightWidth: 1,
        borderTopColor: '#F0F0F0',
        borderRightColor: '#F0F0F0',
        paddingRight: height / 69,
        paddingLeft: height / 69,
        paddingTop: height / 33,
        paddingBottom: height / 33,
        width: width/2,
        alignItems: 'center',
        backgroundColor: COLORS.white
    },
    textreturn: {
        fontSize: height / 45,
        color: COLORS.black,
        fontFamily:'Poppins-Medium'
        
    },
    buttonRight: {
        borderTopWidth: 1,
        borderTopColor: '#F0F0F0',
        paddingRight: height / 69,
        paddingLeft: height / 69,
        paddingTop: height / 33,
        paddingBottom: height / 33,
        width: width/2,
        backgroundColor: COLORS.green,
        alignItems: 'center',
    },
    textrouge: {
        fontSize: height / 45,
        color: COLORS.white,
        
    },
    buttonRightgrey: {
        borderTopWidth: 1,
        borderTopColor: '#F0F0F0',
        paddingRight: height / 69,
        paddingLeft: height / 69,
        paddingTop: height / 33,
        paddingBottom: height / 33,
        width: width/2,
        backgroundColor: COLORS.white,
        alignItems: 'center',
    },
    textblue: {
        fontSize: height / 45,
        color: COLORS.white,
        fontFamily:'Poppins-Medium'
        
    },
    textgrey: {
        fontSize: height / 45,
        color: '#BBBBBB',
        fontFamily:'Poppins-Medium'
        
    }
});