import React, {useState} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  Platform,
  Image,
  TouchableOpacity,
} from 'react-native';

import BottomchapitreIcon from '../Assets/Images/WikiAudio/bottomchapitre.svg';
import TopChapitreIcon from '../Assets/Images/WikiAudio/topChapitre.svg';
import LeftlieuIcon from '../Assets/Images/WikiAudio/leftLieux.svg';
import RightlieuIcon from '../Assets/Images/WikiAudio/RightLieux.svg';
import PlayIcon from '../Assets/Images/WikiAudio/play.svg';
import {useTranslation} from 'react-i18next';
import {styles} from './Styles/TrackPlayer';

const {width, height} = Dimensions.get('screen');

const TrackPlayer = props => {
    const {t} = useTranslation();
    return (
        <View style={styles.musicpanel}>
            <View style={styles.flexDirections}>
              <Text styles={styles.lieutext}>{t('WikiVoicePage.lieux')}</Text>
              <View style={styles.leftdiv}>
                <TouchableOpacity
                  onPress={props.onPress1}>
                  <LeftlieuIcon width={width / 15} height={height / 25} />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={props.onPress2}>
                  <RightlieuIcon width={width / 15} height={height / 25} />
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
            style={styles.divbutton}
                  onPress={props.onPress3}>
                  <PlayIcon width={width / 5} height={height / 12} />
           </TouchableOpacity>
           <View style={styles.flexDirections}>
              <Text styles={styles.lieutext}>{t('WikiVoicePage.chapitres')}</Text>
              <View style={styles.rightdiv}>
                <TouchableOpacity
                  onPress={props.onPress4}>
                  <TopChapitreIcon width={width / 9} height={height / 20} />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={props.onPress5}>
                  <BottomchapitreIcon width={width / 9} height={height / 20} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
    );
  };

export default TrackPlayer;  