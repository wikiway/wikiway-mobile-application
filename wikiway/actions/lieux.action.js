import axios from 'axios';
import {lieuxConstants} from './constants';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const getAllLieux = (radius, thematic, page, longitude, latitude, language, celebrit) => {
  return async dispatch => {
    try {
      dispatch({type: lieuxConstants.GET_ALL_LIEUX_REQUEST});
      const res = await axios.get(`https://api.wikiway.guide/pages`, {
        params: {
          radius,
          thematics: thematic.join(' '),
          pages: page,
          longitude: longitude,
          latitude: latitude,
          languages: language,
          celebrite: celebrit.join(' ')
        },
        
      },);

      if (res.status === 200) {
        const LieuxList = res.data;
        dispatch({
          type: lieuxConstants.GET_ALL_LIEUX_SUCCESS,
          payload: {lieux: LieuxList},
        });
      } else {
        // console.log('loserror',res.data.errors.message);
        const erreur = res.data.errors.message;
        dispatch({
          type: lieuxConstants.GET_ALL_LIEUX_FAILURE,
          payload: {errors: erreur},
        });
      }
    } catch (error) {
      if (error.response) {
        dispatch({
          type: lieuxConstants.GET_ALL_LIEUX_FAILURE,
          payload: {
            errors:
              error.response &&
              error.response.data &&
              error.response.data.errors &&
              error.response.data.errors.message
                ? error.response.data.errors.message
                : error,
          },
        });
        console.log('myerror', error.response.data.errors.message, page);
      }
    }
  };
};


export const getLieuxbyid = (article, language) => {
  return async dispatch => {
    dispatch({type: lieuxConstants.GET_LIEUX_ID_REQUEST}); //http://141.94.209.5:5000/html_parse
    const res = await axios.get(`https://api.wikiway.guide/html_parse`, {
      //10.0.2.2:5000/
      params: {article, languages: language},
      //   params: {article, thematics: thematic.join(' '), languages: 'fr'},
    });

    if (res.status === 200) {
      const LieuxDetail = res.data;
      dispatch({
        type: lieuxConstants.GET_LIEUX_ID_SUCCESS,
        payload: {lieuxdetail: LieuxDetail},
      });
    } else {
      dispatch({
        type: lieuxConstants.GET_LIEUX_ID_FAILURE,
        payload: {error: res.data.error},
      });
    }
  };
};

export const updatetitle = (title, image) => {
  return dispatch => {
    dispatch({type: lieuxConstants.UPDATE_TITLE, payload: {title, image}});
  };
};

export const updateradius = radius => {
  return dispatch => {
    dispatch({type: lieuxConstants.UPDATE_RADIUS, payload: {radius}});
  };
};

export const updatethematic = thematic => {
  return dispatch => {
    dispatch({type: lieuxConstants.GET_THEMATIC, payload: {thematic}});
  };
};

export const updatecelebrite = celebrit => {
  return dispatch => {
    dispatch({type: lieuxConstants.GET_CELEBRITE, payload: {celebrit}});
  };
};

export const updatelonglat = (longitude, latitude) => {
  return dispatch => {
    dispatch({
      type: lieuxConstants.UPDATE_LONGLAT,
      payload: {longitude, latitude},
    });
  };
};

export const articleid = article => {
  return dispatch => {
    dispatch({
      type: lieuxConstants.GET_ARTICLE,
      payload: {article},
    });
  };
};

export const updatelangue = language => {
  console.log('mynlangue',language)
  return async dispatch => {
    dispatch({
      type: lieuxConstants.UPDATE_LANGUE,
      payload: {language},
    });
  };
};

export const pagecounter = page => {
  return async dispatch => {
    dispatch({
      type: lieuxConstants.PAGE_COUNTER,
      payload: {page},
    });
  }
}
