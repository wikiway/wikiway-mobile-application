export const lieuxConstants = {
    GET_ALL_LIEUX_REQUEST: 'GET_ALL_LIEUX_REQUEST',
    GET_ALL_LIEUX_SUCCESS: 'GET_ALL_LIEUX_SUCCESS',
    GET_ALL_LIEUX_FAILURE: 'GET_ALL_LIEUX_FAILURE',
    UPDATE_RADIUS: 'UPDATE_RADIUS',
    GET_THEMATIC: 'GET_THEMATIC',
    GET_CELEBRITE: 'GET_CELEBRITE',
    GET_LIEUX_ID_REQUEST: 'GET_LIEUX_ID_REQUEST',
    GET_LIEUX_ID_SUCCESS: 'GET_LIEUX_ID_SUCCESS',
    GET_LIEUX_ID_FAILURE: 'GET_LIEUX_ID_FAILURE',
    UPDATE_TITLE: 'UPDATE_TITLE',
    UPDATE_LONGLAT:'UPDATE_LONGLAT',
    GET_ARTICLE:'GET_ARTICLE',
    UPDATE_LANGUE:'UPDATE_LANGUE',
    PAGE_COUNTER: 'PAGE_COUNTER',
   
}
