export const COLORS = {
    green: '#45C6BE',
    darkgrey: '#C4C4C4',
    white: 'white',
    black:'black',
    blacklight:'#CFC2C2',
    textColor:'#B4A9A9',
    background: '#F5F5F5',
    thematicTextColor: '#978D8D',
    parametreTextColor: '#3D3636'
}