import 'react-native-gesture-handler';
import * as React from 'react';
import { useEffect } from 'react';
import { View, Text, useWindowDimensions, Alert, BackHandler, Linking } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as Screens from './Screens/AllScreens'
import NavBar from "./Navigation/tabnavigator";
import AsyncStorage from '@react-native-async-storage/async-storage';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';

import af from './Assets/i18n/af/translation.json';
import am from './Assets/i18n/am/translation.json';
import ar from './Assets/i18n/ar/translation.json';
import az from './Assets/i18n/az/translation.json';
import bg from './Assets/i18n/bg/translation.json';
import bn from './Assets/i18n/bn/translation.json';
import bs from './Assets/i18n/bs/translation.json';
import ca from './Assets/i18n/ca/translation.json';
import ceb from './Assets/i18n/ceb/translation.json';
import co from './Assets/i18n/co/translation.json';
import cs from './Assets/i18n/cs/translation.json';
import cy from './Assets/i18n/cy/translation.json';
import da from './Assets/i18n/da/translation.json';
import de from './Assets/i18n/de/translation.json';
import el from './Assets/i18n/el/translation.json';
import en from './Assets/i18n/en/translation.json';
import eo from './Assets/i18n/eo/translation.json';
import es from './Assets/i18n/es/translation.json';
import et from './Assets/i18n/et/translation.json';
import eu from './Assets/i18n/eu/translation.json';
import fa from './Assets/i18n/fa/translation.json';
import fi from './Assets/i18n/fi/translation.json';
import fr from './Assets/i18n/fr/translation.json';
import fy from './Assets/i18n/fy/translation.json';
import ga from './Assets/i18n/ga/translation.json';
import gd from './Assets/i18n/gd/translation.json';
import gl from './Assets/i18n/gl/translation.json';
import gu from './Assets/i18n/gu/translation.json';
import ha from './Assets/i18n/ha/translation.json';
import haw from './Assets/i18n/haw/translation.json';
import hi from './Assets/i18n/hi/translation.json';
import hr from './Assets/i18n/hr/translation.json';
import hu from './Assets/i18n/hu/translation.json';
import ht from './Assets/i18n/ht/translation.json';
import hy from './Assets/i18n/hy/translation.json';
import id from './Assets/i18n/id/translation.json';
import ig from './Assets/i18n/ig/translation.json';
import is from './Assets/i18n/is/translation.json';
import it from './Assets/i18n/it/translation.json';
import iw from './Assets/i18n/iw/translation.json';
import ja from './Assets/i18n/ja/translation.json';
import ka from './Assets/i18n/ka/translation.json';
import kk from './Assets/i18n/kk/translation.json';
import km from './Assets/i18n/km/translation.json';
import kn from './Assets/i18n/kn/translation.json';
import ku from './Assets/i18n/ku/translation.json';
import ky from './Assets/i18n/ky/translation.json';
import la from './Assets/i18n/la/translation.json';
import lb from './Assets/i18n/lb/translation.json';
import lo from './Assets/i18n/lo/translation.json';
import lt from './Assets/i18n/lt/translation.json';
import lv from './Assets/i18n/lv/translation.json';
import mg from './Assets/i18n/mg/translation.json';
import mi from './Assets/i18n/mi/translation.json';
import mk from './Assets/i18n/mk/translation.json';
import ml from './Assets/i18n/ml/translation.json';
import mn from './Assets/i18n/mn/translation.json';
import ms from './Assets/i18n/ms/translation.json';
import mt from './Assets/i18n/mt/translation.json';
import my from './Assets/i18n/my/translation.json';
import ne from './Assets/i18n/ne/translation.json';
import nl from './Assets/i18n/nl/translation.json';
import no from './Assets/i18n/no/translation.json';
import ny from './Assets/i18n/ny/translation.json';
import pl from './Assets/i18n/pl/translation.json';
import ps from './Assets/i18n/ps/translation.json';
import pt from './Assets/i18n/pt/translation.json';
import ro from './Assets/i18n/ro/translation.json';
import ru from './Assets/i18n/ru/translation.json';
import sd from './Assets/i18n/sd/translation.json';
import si from './Assets/i18n/si/translation.json';
import sl from './Assets/i18n/sl/translation.json';
import sk from './Assets/i18n/sk/translation.json';
import sn from './Assets/i18n/sn/translation.json';
import so from './Assets/i18n/so/translation.json';
import sq from './Assets/i18n/sq/translation.json';
import sr from './Assets/i18n/sr/translation.json';
import st from './Assets/i18n/st/translation.json';
import sv from './Assets/i18n/sv/translation.json';
import sw from './Assets/i18n/sw/translation.json';
import ta from './Assets/i18n/ta/translation.json';
import te from './Assets/i18n/te/translation.json';
import tg from './Assets/i18n/tg/translation.json';
import th from './Assets/i18n/th/translation.json';
import tr from './Assets/i18n/tr/translation.json';
import tl from './Assets/i18n/tl/translation.json';
import ur from './Assets/i18n/ur/translation.json';
import uk from './Assets/i18n/uk/translation.json';
import uz from './Assets/i18n/uz/translation.json';
import vi from './Assets/i18n/vi/translation.json';
import xh from './Assets/i18n/xh/translation.json';
import sm from './Assets/i18n/sm/translation.json';
import yi from './Assets/i18n/yi/translation.json';
import yo from './Assets/i18n/yo/translation.json';
import zh from './Assets/i18n/zh/translation.json';
import zu from './Assets/i18n/zu/translation.json';
import VersionCheck from 'react-native-version-check';
import {Provider} from 'react-redux';
import store from './store'
const {Screen,Navigator} = createStackNavigator();

const App = () => {
  const [language, setLanguage] = React.useState("")

  useEffect(() => {
    checkVersion();
  }, [])

  const checkVersion = async () => {
    try {

      let updateNeeded = await VersionCheck.needUpdate();

      console.log(updateNeeded)
      console.log(VersionCheck.getCountry())

      if (updateNeeded && updateNeeded.isNeeded) {

        Alert.alert('Please Update the app','You will have to update your app to the latest version to continue using',[
            {
              text: 'Update',
              onPress: () => {
                BackHandler.exitApp();
                Linking.openURL(updateNeeded.storeUrl);
              },
            }
          ],
          {cancelable: false}
        );
      }
    } catch (error) {}
  }

AsyncStorage.getItem('language').then(value => {
  if (value !== null) {
    setLanguage(value)
    console.log('valeures',value);
  } else {
    AsyncStorage.setItem('language', 'en');
    setLanguage(value)
  }
})

console.log("testtest", language)

const languageDetector = {
  type: 'languageDetector',
  async: true,
  detect: (cb) => cb(language),
  init: () => {},
  cacheUserLanguage: () => {},
};

i18next.use(languageDetector).use(initReactI18next).init({
  fallbackLng: language,
  debug: true,
  resources: {es, it, km, kn, pt, sd, si, sl, sk, sn, so, sr, st, sv, sw, ta, te, tg, th, tr, uk, uz, vi, xh, yi, yo, zu, cy, ga, ha, haw, ht, hu, id, ig, is, iw, ja, kk, fa, gd, ku, ky, la, lb, lo, lt, lv, mi, mk, mg, ml, mn, ms, mt, my, ne, no, pl, ps, ro, sm, af, am, ar, az, bg, bn, bs, ca, ceb, co, cs, da, de, el, en, eo, et, eu, fr, fi, fy, gl, gu, hi, hr, hy, ka, nl, ny, ru, sq, tl, ur, zh },
});

  return (
    <Provider store={store}>
    <NavigationContainer>
      <Navigator
        screenOptions={{headerShown: false,
        }}>
      <Screen name="SplashScreen" component={Screens.SplashScreen} options={{gestureEnabled: false}} />
      <Screen name="DescThematic" component={Screens.DescThematic} options={{gestureEnabled: false}} />
      <Screen name="Sliders" component={Screens.Sliders} options={{gestureEnabled: false}} />
      <Screen name="Sliders2" component={Screens.Sliders2} options={{gestureEnabled: false}} />
      <Screen name="Thematic" component={Screens.Thematic} options={{gestureEnabled: false}} />
      <Screen name="Home" component={Screens.Home} options={{gestureEnabled: false}} />
      <Screen name="NavBar" component={NavBar} options={{gestureEnabled: false}}/>
      <Screen name="detailLieu" component={Screens.detailLieu} options={{gestureEnabled: false}}/>
      <Screen name="Form" component={Screens.Form} options={{gestureEnabled: false}}/>
      <Screen name="Langue" component={Screens.Langue} options={{gestureEnabled: false}}/>
      <Screen name="Luminosite" component={Screens.Luminosite} options={{gestureEnabled: false}}/>
      <Screen name="PolitiqueConfidentialite" component={Screens.PolitiqueConfidentialite} options={{gestureEnabled: false}}/>
      <Screen name="ConditionUtilisation" component={Screens.ConditionUtilisation} options={{gestureEnabled: false}}/>
      <Screen name="MentionsLegales" component={Screens.MentionsLegales} options={{gestureEnabled: false}}/>
      <Screen name="WebView1" component={Screens.WebView1} options={{gestureEnabled: false}}/>
      </Navigator>
    </NavigationContainer>
    </Provider>
  );
}
/*<Screen name="Voix" component={Screens.Voix} options={{gestureEnabled: false}}/>
<Screen name="Browser" component={Screens.Browser} options={{gestureEnabled: false}}/>*/
export default App;





