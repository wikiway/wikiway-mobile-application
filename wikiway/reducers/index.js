import { combineReducers } from "redux";
import lieuxReducer from './lieux.reducers'

const rootReducer = combineReducers({
 lieux:lieuxReducer,
})

export default rootReducer;