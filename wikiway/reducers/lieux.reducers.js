import {lieuxConstants} from '../actions/constants';

const initState = {
  lieux: [],
  lieuxdetail: [],
  radius: 5,
  loading: false,
  error: null,
  thematic: [],
  title: null,
  longitude: null,
  latitude: null,
  article: null,
  celebrit: '',
  page: 0
};

export default (state = initState, action) => {
  switch (action.type) {
    case lieuxConstants.GET_ALL_LIEUX_REQUEST:
      state = {
        ...state,
        loading: true,
      };
      break;
    case lieuxConstants.GET_ALL_LIEUX_SUCCESS:
      state = {
        ...state,
        lieux: action.payload.lieux,
        loading: false,
        error: null,
      };
      break;
    case lieuxConstants.GET_ALL_LIEUX_FAILURE:
      state = {
        ...state,
        loading: false,
        error: action.payload.errors
      };
      break;
    case lieuxConstants.UPDATE_RADIUS:
      state = {
        ...state,
        radius: action.payload.radius,
      };
      break;
    case lieuxConstants.GET_THEMATIC:
      state = {
        ...state,
        thematic: action.payload.thematic,
      };
      break;
    case lieuxConstants.GET_CELEBRITE:
      state = {
        ...state,
        celebrit: action.payload.celebrit,
      };
      break;
    case lieuxConstants.GET_LIEUX_ID_REQUEST:
      state = {
        ...state,
        loading: true,
      };
      break;
    case lieuxConstants.GET_LIEUX_ID_SUCCESS:
      state = {
        ...state,
        lieuxdetail: action.payload.lieuxdetail,
        loading: false,
      };
      break;
    case lieuxConstants.GET_LIEUX_ID_FAILURE:
      state = {
        ...state,
        loading: true,
      };
      break;  
    case lieuxConstants.UPDATE_TITLE:
      state = {
        ...state,
        title: action.payload.title,
        image: action.payload.image
      };
      break;
    case lieuxConstants.UPDATE_LONGLAT:
      state = {
        ...state,
        longitude: action.payload.longitude,
        latitude: action.payload.latitude,
      };
      // console.log('mystate',state.longitude,state.latitude)
      
      break;
    case lieuxConstants.GET_ARTICLE:
      state = {
        ...state,
        article: action.payload.article,
      };
      break;
    case lieuxConstants.UPDATE_LANGUE:
      state = {
        ...state,
        language: action.payload.language,
      };
      console.log('mystatelangue2',state.language)
      break;
    case lieuxConstants.PAGE_COUNTER:
      state = {
        ...state,
        page: action.payload.page,
      };
      console.log('mypage')
      break;
  }
  return state;
};
